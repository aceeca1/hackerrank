object Solution {
    import io.StdIn._

    def main(args:Array[String]) {
        val reNum = "[+-]?[1-9][0-9]*(\\.[0-9]+)?"
        val re = ("\\((" + reNum + "), (" + reNum + ")\\)").r
        for (_ <- 1 to readInt) {
            println(readLine match {
                case re(x, _, y, _) if {
                    val xD = x.toDouble
                    val yD = y.toDouble
                    -90.0 <= xD && xD <= 90.0 && -180.0 <= yD && yD <= 180.0
                } => "Valid"
                case _ => "Invalid"
            })
        }
    }
}
