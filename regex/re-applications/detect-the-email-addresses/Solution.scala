object Solution {
    import io.StdIn._

    val re = "[^ \t]+@([a-z0-9-]+\\.)*[a-z0-9-]+".r

    def main(args:Array[String]) {
        val s = Array.fill(readInt){readLine}.mkString(" ")
        val b = re.findAllIn(s).toBuffer
        println(b.sorted.distinct.mkString(";"))
    }
}
