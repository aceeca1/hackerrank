object Solution {
    import io.StdIn._

    def main(args:Array[String]) {
        val s = Iterator.fill(readInt){readLine}.mkString(" ")
        for (_ <- 1 to readInt) {
            val re = ("\\b" + readLine + "\\b").r
            println(re.findAllIn(s).size)
        }
    }
}
