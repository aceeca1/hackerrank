object Solution {
    import io.StdIn._

    val re = "(?m)(?s)'.'|\".*?\"|/\\*.*?\\*/|//.*?$".r

    def main(args:Array[String]) {
        val s = io.Source.stdin.mkString
        for (i <- re.findAllIn(s)) if (i.head == '/') {
            println("\n[ \t]+".r.replaceAllIn(i, "\n"))
        }
    }
}
