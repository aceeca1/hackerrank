object Solution {
    import io.StdIn._

    val re = "://([a-z0-9\\.-]+\\.[a-z]+)".r

    def main(args:Array[String]) {
        val s = io.Source.stdin.mkString
        val b = re.findAllMatchIn(s).map{_.group(1) match {
            case x if (
                x.startsWith("www.") || x.startsWith("ww2.")
            ) => x.substring(4, x.size)
            case x => x
        }}.toBuffer
        println(b.sorted.distinct.mkString(";"))
    }
}
