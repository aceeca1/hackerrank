object Solution {
    import io.StdIn._
    import collection.mutable.Buffer

    val reTag = "<([^/][^>]*)>".r
    val reAttr = "([a-z]+)=(\"[^\"]*\"|'[^']*')".r

    def main(args:Array[String]) {
        val s = io.Source.stdin.mkString
        val b = Buffer[(String, String)]()
        for (iTag <- reTag.findAllMatchIn(s)) {
            val tag = iTag.group(1)
            val tagName = tag.takeWhile{_ != ' '}
            val aAttr = reAttr.findAllMatchIn(tag)
            if (!aAttr.hasNext) b.+=((tagName, ""))
            for (iAttr <- aAttr) b.+=((tagName, iAttr.group(1)))
        }
        val a = b.sorted
        var (v0, k0) = a.head
        def printBeginner {print(v0); print(':'); print(k0)}
        printBeginner
        for ((v, k) <- a.toIterator.drop(1)) if (v != v0) {
            v0 = v
            k0 = k
            println
            printBeginner
        } else if (k != k0) {
            k0 = k
            print(',')
            print(k)
        }
        println
    }
}
