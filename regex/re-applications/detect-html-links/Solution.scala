object Solution {
    import io.StdIn._

    val re = "<a( [^>]*)? href=\"([^\"]+)\"( [^>]*)?>(.*?)</a>".r

    def main(args:Array[String]) {
        val s = io.Source.stdin.mkString
        for (i <- re.findAllMatchIn(s)) {
            print(i.group(2).trim)
            print(',')
            println("<.+?>".r.replaceAllIn(i.group(4), "").trim)
        }
    }
}
