object Solution {
    import io.StdIn._
    import collection.mutable.Buffer

    val re = "<([^/][^> ]*)[^>]*>".r

    def main(args:Array[String]) {
        val s = io.Source.stdin.mkString
        val b = re.findAllMatchIn(s).map{_.group(1)}.toBuffer
        println(b.sorted.distinct.mkString(";"))
    }
}
