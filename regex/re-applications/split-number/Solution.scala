object Solution {
    import io.StdIn._

    def main(args:Array[String]) {
        val re = "([0-9]+)[ -]([0-9]+)[ -]([0-9]+)".r
        for (_ <- 1 to readInt) {
            val re(cc, lac, n) = readLine
            print("CountryCode=")
            print(cc)
            print(",LocalAreaCode=")
            print(lac)
            print(",Number=")
            println(n)
        }
    }
}
