object Solution {
    import io.StdIn._

    val reId = "/questions/([0-9]+)/".r
    val reQ = "<h3><a[^>]+>([^<]+)<".r
    val reT = "class=\"relativetime\">([^<]+)</span>".r
    val re = Array(reId, reQ, reT)

    def main(args:Array[String]) {
        val s = io.Source.stdin.mkString
        val id = reId.findAllMatchIn(s).map{_.group(1)}
        val question = reQ.findAllMatchIn(s).map{_.group(1)}
        val time = reT.findAllMatchIn(s).map{_.group(1)}
        for (i <- id) {
            print(i)
            print(';')
            print(question.next)
            print(';')
            println(time.next)
        }
    }
}
