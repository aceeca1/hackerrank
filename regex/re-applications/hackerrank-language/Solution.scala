object Solution {
    import io.StdIn._

    def main(args:Array[String]) {
        for (_ <- 1 to readInt) {
            println(readLine.split(' ')(1) match {
                case (
                    "C" | "CPP" | "JAVA" | "PYTHON" | "PERL" | "PHP" |
                    "RUBY" | "CSHARP" | "HASKELL" | "CLOJURE" | "BASH" |
                    "SCALA" | "ERLANG" | "CLISP" | "LUA" | "BRAINFUCK" |
                    "JAVASCRIPT" | "GO" | "D" | "OCAML" | "R" | "PASCAL" |
                    "SBCL" | "DART" | "GROOVY" | "OBJECTIVEC"
                ) => "VALID"
                case _ => "INVALID"
            })
        }
    }
}
