object Solution {
    import io.StdIn._

    def main(args:Array[String]) {
        for (_ <- 1 to readInt) {
            val s = readLine
            val sL = s.toLowerCase
            if (sL.startsWith("hi ") && sL(3) != 'd') println(s)
        }
    }
}
