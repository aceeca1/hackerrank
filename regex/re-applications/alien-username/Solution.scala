object Solution {
    import io.StdIn._
    import collection.mutable.Buffer

    val re = "[_\\.][0-9]+[A-Za-z]*_?".r

    def main(args:Array[String]) {
        for (_ <- 1 to readInt) {
            println(readLine match {
                case re() => "VALID"
                case _ => "INVALID"
            })
        }
    }
}
