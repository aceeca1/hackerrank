object Solution {
    import io.StdIn._

    def main(args:Array[String]) {
        for (_ <- 1 to readInt) {
            val s = readLine
            println(if (s.startsWith("hackerrank")) {
                if (s.endsWith("hackerrank")) 0 else 1
            } else {
                if (s.endsWith("hackerrank")) 2 else -1
            })
        }
    }
}
