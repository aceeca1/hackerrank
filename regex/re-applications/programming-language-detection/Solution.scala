object Solution {
    import io.StdIn._

    def main(args:Array[String]) {
        println(io.Source.stdin.mkString match {
            case s if s.contains("public static ") => "Java"
            case s if s.contains("import ") => "Java"
            case s if s.contains("#include") => "C"
            case s if s.contains("int main()") => "C"
            case _ => "Python"
        })
    }
}
