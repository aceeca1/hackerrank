object Solution {
    import io.StdIn._

    val re = "[A-Z][A-Z][A-Z][A-Z][A-Z][0-9][0-9][0-9][0-9][A-Z]".r

    def main(args:Array[String]) {
        for (_ <- 1 to readInt) {
            println(readLine match {
                case re() => "YES"
                case _ => "NO"
            })
        }
    }
}
