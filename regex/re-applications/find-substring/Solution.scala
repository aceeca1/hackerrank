object Solution {
    import io.StdIn._
    import collection.mutable.Buffer

    val re = "<([^/][^> ]*)[^>]*>".r

    def main(args:Array[String]) {
        val s = Iterator.fill(readInt){readLine}.mkString(" ")
        for (_ <- 1 to readInt) {
            val re = ("[a-z0-9_]" + readLine + "[a-z0-9_]").r
            println(re.findAllIn(s).size)
        }
    }
}
