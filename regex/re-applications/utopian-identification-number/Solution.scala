object Solution {
    import io.StdIn._

    def main(args:Array[String]) {
        val re1 = "[a-z]?" * 3 + "[0-9]" * 2 + "[0-9]?" * 6
        val re2 = "[A-Z]" * 3 + "[A-Z]*"
        val re = (re1 + re2).r
        for (_ <- 1 to readInt) {
            println(readLine match {
                case re() => "VALID"
                case _ => "INVALID"
            })
        }
    }
}
