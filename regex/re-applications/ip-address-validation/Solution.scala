object Solution {
    import io.StdIn._

    /* Don't use it! See
     * http://www.oracle.com/technetwork/java/faq-sun-packages-142232.html */
    import sun.net.util.IPAddressUtil._

    def main(args:Array[String]) {
        for (_ <- 1 to readInt) {
            println(readLine match {
                case s if isIPv6LiteralAddress(s) => "IPv6"
                case s if isIPv4LiteralAddress(s) => "IPv4"
                case _ => "Neither"
            })
        }
    }
}
