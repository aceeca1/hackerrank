object Solution {
    import io.StdIn._

    def main(args:Array[String]) {
        val s = Array.fill(readInt){readLine}.mkString(" ")
        for (_ <- 1 to readInt) {
            val w = readLine
            val re = ("\\b(" + w + "|" + w.dropRight(2) + "se)\\b").r
            println(re.findAllIn(s).size)
        }
    }
}
