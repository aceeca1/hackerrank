object Solution {
    import io.StdIn._

    def main(args:Array[String]) {
        val s = Array.fill(readInt){readLine}.mkString(" ")
        for (_ <- 1 to readInt) {
            val w = readLine
            val v = w.replace("our", "or")
            val re = ("\\b(" + v + "|" + w + ")\\b").r
            println(re.findAllIn(s).size)
        }
    }
}
