object Solution {
    import io.StdIn._

    def main(args:Array[String]) {
        val a = Iterator.fill(readInt){readLine}
        println(a.count{_.toLowerCase.contains("hackerrank")})
    }
}
