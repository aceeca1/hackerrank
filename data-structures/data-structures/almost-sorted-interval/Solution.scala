object Solution {
    import io.StdIn._
    import collection.mutable.Buffer

    def main(args:Array[String]) {
        val n = readInt
        val a = readLine.split(' ').map{_.toInt}
        val z = Array.fill(n + 2){0}
        for (i <- a.indices) z(a(i)) = i
        z(0) = n
        z(n + 1) = -1
        val m = 1 << (Math.getExponent(n.toFloat) + 1)
        val b = Array.fill(m + m, 0){0}
        val s1 = Buffer(0)
        for (i <- (n - 1).to(0, -1)) {
            while (s1.last > a(i)) s1.remove(s1.size - 1)
            s1.+=(a(i))
            b(m + i) = Array(z(s1(s1.size - 2)))
        }
        for (i <- (m - 1).to(1, -1)) b(i) = merge(b(i + i), b(i + i + 1))
        val s2 = Buffer(n + 1)
        var s = 0L
        for (i <- 0.until(n)) {
            while (s2.last < a(i)) s2.remove(s2.size - 1)
            s2.+=(a(i))
            var p = m + z(s2(s2.size - 2))
            var q = m + i
            while ((p ^ q) != 1) {
                if ((p & 1) == 0) {
                    val u = b(p + 1)
                    s = s + u.size - BinarySearch(0, u.size){u(_) > i}
                }
                if ((q & 1) == 1) {
                    val u = b(q - 1)
                    s = s + u.size - BinarySearch(0, u.size){u(_) > i}
                }
                p = p >>> 1
                q = q >>> 1
            }
        }
        println(s + n)
    }

    def merge(a1:Array[Int], a2:Array[Int]) = {
        val a = Array.ofDim[Int](a1.size + a2.size)
        System.arraycopy(a1, 0, a, 0, a1.size)
        System.arraycopy(a2, 0, a, a1.size, a2.size)
        java.util.Arrays.sort(a); a
    }

    object BinarySearch {
        def apply(x1:Int, x2:Int)(f:Int => Boolean):Int = {
            if (x1 == x2) return x1
            val m = (x1 + x2) >>> 1
            if (f(m)) apply(x1, m)(f) else apply(m + 1, x2)(f)
        }
    }
}

