struct Node {
    int data;
    Node *next;
};

#include <cstdio>
Node* Reverse(Node* head) {
    Node *prev = nullptr;
    while (head) {
        Node *next = head->next;
        head->next = prev;
        prev = head;
        head = next;
    }
    return prev;
}
