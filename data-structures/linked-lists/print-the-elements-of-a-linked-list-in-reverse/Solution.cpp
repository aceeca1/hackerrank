struct Node {
    int data;
    Node *next;
};

#include <cstdio>
void ReversePrint(Node *head) {
    Node *prev = nullptr;
    while (head) {
        Node *next = head->next;
        head->next = prev;
        prev = head;
        head = next;
    }
    for (; prev; prev = prev->next) {
        std::printf("%d\n", prev->data);
    }
}
