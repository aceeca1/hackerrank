struct Node {
    int data;
    Node *next;
};

Node* Delete(Node *head, int position) {
    Node **h = &head;
    while (position--) h = &(*h)->next;
    *h = (*h)->next;
    return head;
}
