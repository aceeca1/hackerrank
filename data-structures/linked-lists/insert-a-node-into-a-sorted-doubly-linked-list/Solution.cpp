struct Node {
    int data;
    Node *next, *prev;
};

Node* SortedInsert(Node *head, int data) {
    if (!head) return new Node{data, nullptr, nullptr};
    if (data < head->data) {
        Node *p = new Node{data, head, nullptr};
        head->prev = p;
        return p;
    }
    Node *i = head;
    while (i->next && data > i->next->data) i = i->next;
    i->next = new Node{data, i->next, i};
    if (i->next->next) i->next->next->prev = i->next;
    return head;
}
