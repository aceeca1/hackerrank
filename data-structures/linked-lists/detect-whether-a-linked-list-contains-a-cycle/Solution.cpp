struct Node {
    int data;
    Node *next;
};

int HasCycle(Node* head) {
    int n1 = 0, n2 = 1;
    if (!head) return 0;
    Node *x1 = head, *x2 = head->next;
    while (x1 != x2) {
        if (!x2) return 0;
        if (n2 > n1 + n1) {
            n1 = n2;
            x1 = x2;
        }
        ++n2;
        x2 = x2->next;
    }
    return 1;
}
