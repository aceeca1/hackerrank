struct Node {
    int data;
    Node *next;
};

int FindMergeNode(Node* headA, Node* headB) {
    int sA = 0;
    for (Node *i = headA; i; i = i->next) ++sA;
    int sB = 0;
    for (Node *i = headB; i; i = i->next) ++sB;
    Node *pA = headA, *pB = headB;
    for (; sA > sB; --sA) pA = pA->next;
    for (; sB > sA; --sB) pB = pB->next;
    while (pA != pB) {
        pA = pA->next;
        pB = pB->next;
    }
    return pA->data;
}
