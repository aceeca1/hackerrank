struct Node {
    int data;
    Node *next;
};

Node* MergeLists(Node* headA, Node* headB) {
    Node head, *p = &head;
    while (headA && headB) {
        if (headA->data < headB->data) {
            p = p->next = headA;
            headA = headA->next;
        } else {
            p = p->next = headB;
            headB = headB->next;
        }
    }
    for (; headA; headA = headA->next) p = p->next = headA;
    for (; headB; headB = headB->next) p = p->next = headB;
    return head.next;
}
