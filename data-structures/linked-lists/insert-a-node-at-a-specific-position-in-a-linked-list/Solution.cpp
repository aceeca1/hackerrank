struct Node {
    int data;
    Node *next;
};

Node* InsertNth(Node *head, int data, int position) {
    Node **i = &head;
    while (position--) i = &(*i)->next;
    *i = new Node{data, *i};
    return head;
}
