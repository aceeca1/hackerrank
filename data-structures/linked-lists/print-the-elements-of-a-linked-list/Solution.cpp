struct Node {
    int data;
    Node *next;
};

#include <cstdio>
void Print(Node *head) {
    for (; head; head = head->next) {
        std::printf("%d\n", head->data);
    }
}
