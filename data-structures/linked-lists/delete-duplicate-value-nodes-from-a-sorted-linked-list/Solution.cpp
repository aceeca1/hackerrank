struct Node {
    int data;
    Node *next;
};

Node* RemoveDuplicates(Node *head) {
    for (Node *i = head; i->next;) {
        if (i->data == i->next->data) i->next = i->next->next;
        else i = i->next;
    }
    return head;
}
