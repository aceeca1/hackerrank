struct Node {
    int data;
    Node *next;
};

int GetNode(Node *head, int positionFromTail) {
    for (Node *i = head; i; i = i->next) --positionFromTail;
    while (++positionFromTail) head = head->next;
    return head->data;
}
