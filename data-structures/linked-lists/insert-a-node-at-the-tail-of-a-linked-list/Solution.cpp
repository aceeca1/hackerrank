struct Node {
    int data;
    Node *next;
};

Node* Insert(Node *head, int data) {
    Node **i = &head;
    while (*i) i = &(*i)->next;
    *i = new Node{data, nullptr};
    return head;
}
