struct Node {
    int data;
    Node *next;
};

Node* Insert(Node *head, int data) {
    return new Node{data, head};
}
