struct Node {
    int data;
    Node *next;
};

int CompareLists(Node *headA, Node* headB) {
    for(; headA; headA = headA->next) {
        if (!headB) return 0;
        if (headA->data != headB->data) return 0;
        headB = headB->next;
    }
    return !headB;
}
