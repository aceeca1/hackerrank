#include <cstdio>
#include <vector>
#include <algorithm>
using namespace std;

struct VP: public vector<int> {
    VP() {
        for (int i1 = 0; i1 < 0xc0; i1 += 0x40)
            for (int i2 = i1; i2 < i1 + 0x30; i2 += 0x10)
                for (int i3 = i2; i3 < i2 + 0x0c; i3 += 0x04)
                    for (int i4 = i3; i4 < i3 + 0x03; ++i4)
                        push_back(i4);
    }
} vP;

struct Board {
    vector<int> a = vector<int>(0x100, 0x1ff);
    vector<int> w = vector<int>(0x100, 11);
    Board(void*) {}
    Board() {
        for (int i: vP) w[i] = 9;
    }
    bool isNull() {return w[0] == 11;}
} nullBoard(nullptr);

Board guess(Board b, int p, int n) {
    Board d;
    for (int i: vP) if ((b.a[i] & n) && (
        (i & 0xf0) == (p & 0xf0) ||
        (i & 0x0f) == (p & 0x0f) ||
        (i & 0xcc) == (p & 0xcc)
    )) {
        d.a[i] = b.a[i] - n;
        d.w[i] = b.w[i] - 1;
    } else {
        d.a[i] = b.a[i];
        d.w[i] = b.w[i];
    }
    d.a[p] = n;
    d.w[p] = 10;
    return d;
}

Board solve(Board b) {
    int p = min_element(b.w.begin(), b.w.end()) - b.w.begin();
    if (b.w[p] == 0) return nullBoard;
    if (b.w[p] == 10) return b;
    for (int a = b.a[p]; a;) {
        int lB = a & -a;
        Board d = solve(guess(b, p, lB));
        if (!d.isNull()) return d;
        a = a - lB;
    }
    return nullBoard;
}

int main() {
    int t;
    scanf("%d", &t);
    while (t--) {
        Board b;
        for (int i = 0; i < 81; ++i) {
            int n;
            scanf("%d", &n);
            if (n == 0) continue;
            b = guess(b, vP[i], 1 << (n - 1));
        }
        b = solve(b);
        for (int i1 = 0; i1 < 81; i1 += 9) {
            for (int i2 = i1; i2 < i1 + 9; ++i2) {
                if (i2 != i1) printf(" ");
                switch (b.a[vP[i2]]) {
                    case 0x001: printf("1"); break;
                    case 0x002: printf("2"); break;
                    case 0x004: printf("3"); break;
                    case 0x008: printf("4"); break;
                    case 0x010: printf("5"); break;
                    case 0x020: printf("6"); break;
                    case 0x040: printf("7"); break;
                    case 0x080: printf("8"); break;
                    case 0x100: printf("9"); break;
                }
            }
            printf("\n");
        }
    }
    return 0;
}
