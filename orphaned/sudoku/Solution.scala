object Solution {
    import io.StdIn._

    val vP = (for {
        i1 <- Range(0, 0xc0, 0x40)
        i2 <- Range(i1, i1 + 0x30, 0x10)
        i3 <- Range(i2, i2 + 0x0c, 0x04)
        i4 <- Range(i3, i3 + 0x03)
    } yield i4).toArray

    class Board {
        val a = Array.fill(0x100){0x1ff}
        val w = Array.fill(0x100){11}
        for (i <- vP) w(i) = 9
    }
    
    def guess(b:Board, p:Int, n:Int) = {
        val d = new Board
        for (i <- vP) if (((b.a(i) & n) > 0) && (
            (i & 0xf0) == (p & 0xf0) ||
            (i & 0x0f) == (p & 0x0f) ||
            (i & 0xcc) == (p & 0xcc)
        )) {
            d.a(i) = b.a(i) - n
            d.w(i) = b.w(i) - 1
        } else {
            d.a(i) = b.a(i)
            d.w(i) = b.w(i)
        }
        d.a(p) = n
        d.w(p) = 10
        d
    }
    
    def solve(b:Board):Board = {
        val p = b.w.indices.minBy(b.w)
        if (b.w(p) == 0) return null
        if (b.w(p) == 10) return b
        var a = b.a(p)
        while (a > 0) {
            val lB = a & -a
            val d = solve(guess(b, p, lB))
            if (d != null) return d
            a = a - lB
        }
        return null
    }
    
    def main(args:Array[String]) {
        for (_ <- 1 to readInt) {
            var b = new Board
            for {
                i1 <- Range(0, 81, 9)
                it = readLine.split(' ').map{_.toInt}.toIterator
                i2 <- Range(i1, i1 + 9)
            } it.next match {
                case 0 => ()
                case n => b = guess(b, vP(i2), 1 << (n - 1))
            }
            b = solve(b)
            for (i1 <- Range(0, 81, 9)) {
                println(Range(i1, i1 + 9).view.map{i => b.a(vP(i)) match {
                    case 0x001 => 1; case 0x002 => 2; case 0x004 => 3
                    case 0x008 => 4; case 0x010 => 5; case 0x020 => 6
                    case 0x040 => 7; case 0x080 => 8; case 0x100 => 9
                }}.mkString(" "))
            }
        }
    }
}
