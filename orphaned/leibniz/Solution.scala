object Solution {
    import io.StdIn._
    import Math._

    def main(args:Array[String]) {
        for (_ <- 1 to readInt) {
            val x = readInt
            def f(s:Double, i:Int):Double = {
                if (i < 0) return s
                f(1.0 / i - s, i - 2)
            }
            println(abs(f(0.0, x + x - 1)).formatted("%.15f"))
        }
    }
}
