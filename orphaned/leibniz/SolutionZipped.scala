object Solution{def main(args:Array[String]){for(_<-1 to readInt){val x=readInt;def f(s:Double,i:Int):Double=if(i<0)s else f(1.0/i-s,i-2);println(Math.abs(f(0,x+x-1)).formatted("%.15f"))}}}
