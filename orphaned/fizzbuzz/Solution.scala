object Solution {
    import io.StdIn._

    def main(args:Array[String]) {
        1.to(100).map{
            case i if i % 3 == 0 && i % 5 == 0 => println("FizzBuzz")
            case i if i % 3 == 0 => println("Fizz")
            case i if i % 5 == 0 => println("Buzz")
            case i => println(i)
        }
    }
}
