object Bezout {
    def divideBy(u:Long, v:Long, m:Long, a:Long = 1L, b:Long = 0L):Long = {
        if (v == 0L) return (u / m * b % a + Math.abs(a)) % a
        val k = m / v
        divideBy(u, m - v * k, v, b - a * k, a)
    }
}
