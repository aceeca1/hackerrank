import collection.mutable.Buffer
import collection.{BufferedIterator => BIter}

object Prime {
    val p = Array.fill(65536){0}; {
        def g(u:Int)(i:Int) {
            def f(v:Int)(w:Int) {
                if (i * w >= 65536) return;
                p(i * w) = w
                if (w < v) f(v)(p(w))
            }
            if (i < 65536) p(i) match {
                case 0 => {p(u) = i; f(i)(2); g(i)(i + 1)}
                case v => {f(v)(2); g(u)(i + 1)}
            } else p(u) = 65536
        }; g(0)(2)
    }

    def is(n:Int):Boolean = {
        if (n < 65536) return p(n) > n
        def f(i:Int):Boolean = {
            if (i * i > n) return true
            if (n % i == 0) return false
            f(p(i))
        }; f(2)
    }

    def minFactor(n:Int):Int = {
        if (n < 65536) return if (p(n) < n) p(n) else n
        def f(i:Int):Int = {
            if (i * i > n) return n
            if (n % i == 0) return i
            f(p(i))
        }; f(2)
    }

    def factorize(n:Int):Buffer[Int] = {
        val b = Buffer[Int]()
        def f(i:Int, n:Int):Int = {
            if (n < 65536) return n
            else if (i * i > n) {b += n; return 1}
            else if (n % i == 0) {b += i; f(i, n / i)}
            else f(p(i), n)
        }
        def g(n:Int) {
            if (p(n) > n) {b += n; return}
            b += p(n); g(n / p(n))
        }
        f(2, n) match {
            case 1 => b
            case n => {g(n); b}
        }
    }
}; case class Prime(var head:Int = 2) extends BIter[Int] {
    import Prime._

    def hasNext = true
    def next:Int = {
        val head0 = head
        def f(n:Int):Int = {
            if (is(n)) return n
            f(n + 2)
        }
        if (head < 65521) head = p(head)
        else head = f(head + 2)
        head0
    }
}
