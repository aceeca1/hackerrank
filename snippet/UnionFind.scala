class UnionFind(n:Int) {
    val a = Array.fill(n + 1){-1}
    def find(p:Int):Int = {
        var c0 = -1
        var c1 = p
        while (a(c1) != -1) {
            val c2 = a(c1)
            a(c1) = c0
            c0 = c1
            c1 = c2
        }
        while (c0 != -1) {
            val c2 = a(c0)
            a(c0) = c1
            c0 = c2
        }; c1
    }
    def union(p:Int, q:Int) {
        val p0 = find(p)
        val q0 = find(q)
        if (p0 == q0) return
        a(p0) = q0
    }
}
