case class Trie(n:Int) {
    var value = -1
    val ch = Array.fill(n){null:Trie}
    def add(a:Iterator[Int], v:Int) {
        var p = this
        for (ai <- a) {
            if (p.ch(ai) == null) p.ch(ai) = Trie(n)
            p = p.ch(ai)
        }
        p.value = v
    }
    def aho() {
        val p = Trie(n); for (i <- 0 until n) p.ch(i) = this
        val pp = Trie(n); pp.ch(0) = p
        val q = collection.mutable.Queue((this, pp, 0))
        while (q.nonEmpty) {
            val (u, vP, w) = q.dequeue
            val v = vP.ch(w)
            for (i <- 0 until n) {
                if (u.ch(i) == null) u.ch(i) = v.ch(i)
                else q.enqueue((u.ch(i), v, i))
            }
        }
    }
    def foreachMatchIn(a:Iterator[Int])(f:(Int, Int) => Unit) {
        var i = -1
        var p = this
        for (ai <- a) {
            p = p.ch(ai)
            i = i + 1
            if (p.value != -1) f(p.value, i)
        }
    }
}
