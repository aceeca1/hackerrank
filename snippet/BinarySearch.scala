object BinarySearch {
    def apply(x1:Int, x2:Int)(f:Int => Boolean):Int = {
        if (x1 == x2) return x1
        val m = (x1 + x2) >>> 1
        if (f(m)) apply(x1, m)(f) else apply(m + 1, x2)(f)
    }
}
