object ReadArrayInt {
    import io.StdIn._
    def apply(n:Int):Array[Int] = {
        val in = readLine
        var p = 0
        val a = Array.fill(n){0}
        var q = 0
        while (q < n) {
            while (in(p).isWhitespace) p = p + 1
            var x = 0
            var neg = false
            if (in(p) == '-') {
                neg = true
                p = p + 1
            }
            while (p < in.size && in(p).isDigit) {
                x = x * 10 + (in(p) - '0')
                p = p + 1
            }
            a(q) = if (neg) -x else x
            q = q + 1
        }; a
    }
}
