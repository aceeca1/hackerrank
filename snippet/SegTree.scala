object SegTree {
    case class Node(var v:Int, var l:Node, var r:Node)
    var m = -1
    def setM(mv:Int) {m = Math.getExponent(mv)}
    val nulNode = Node(0, null, null)
    val nulTree = SegTree(SegTree.nulNode)
    def newNode = Node(0, nulNode, nulNode)
}
case class SegTree(root:SegTree.Node = SegTree.newNode) {
    import SegTree._
    def push(n:Int, d:Int = 1, h:Int = m, a:Node = root) {
        a.v = a.v + d
        if (h == -1) return
        if (((n >>> h) & 1) == 0) {
            if (a.l eq nulNode) a.l = newNode
            push(n, d, h - 1, a.l)
        } else {
            if (a.r eq nulNode) a.r = newNode
            push(n, d, h - 1, a.r)
        }
    }
    def select(k:Int, n:Int = 0, h:Int = m, a:Node = root):Int = {
        if (h == -1 || a == nulNode) return n
        if (k <= a.l.v) select(k, n, h - 1, a.l)
        else select(k - a.l.v, n + (1 << h), h - 1, a.r)
    }
    def median = select(root.v + 1 >>> 1)
    def merge(b:SegTree) = {
        def m(n1:Node, n2:Node):Node = {
            if (n1 == nulNode) return n2
            if (n2 == nulNode) return n1
            Node(n1.v + n2.v, m(n1.l, n2.l), m(n1.r, n2.r))
        }; new SegTree(m(root, b.root))
    }
}
