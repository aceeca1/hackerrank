case class Phi(n:Int, minFactor:(Int => Int)) {
    def a = Array.fill(n + 1){0}; a(1) = 1
    for (i <- 2 to n) {
        val p = minFactor(i)
        val q = i / p
        val r = minFactor(q)
        a(i) = a(q) * (if (p == r) p else p - 1)
    }
}
