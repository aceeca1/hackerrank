case class MaxFlow(v:Int) {
    case class E(t:Int, var u:Int, var pair:E)
    val e = Array.fill(v){Buffer[E]()}
    val ei = Array.fill(v){0}
    val h = Array.fill(v){0}
    val hS = Array.fill(v + 1){0}
    hS(0) = v
    var vS = 0
    var vT = 0

    def addEdge(s:Int, t:Int, u:Int) {
        val eS = E(t, u, null)
        val eT = E(s, 0, eS)
        eS.pair = eT
        e(s).+=(eS)
        e(t).+=(eT)
    }

    def aug(p:Int, q:Int):Int = {
        if (p == vT) return q
        var qL = q
        while (ei(p) < e(p).size) {
            val i = e(p)(ei(p))
            if (i.u != 0 && h(i.t) + 1 == h(p)) {
                val d = aug(i.t, qL.min(i.u))
                i.u = i.u - d
                i.pair.u = i.pair.u + d
                qL = qL - d
                if (h(vS) == v || qL == 0) return q - qL
            }
            ei(p) = ei(p) + 1
        }
        ei(p) = 0
        var mH = v - 1
        for (i <- e(p)) if (i.u != 0) mH = mH.min(h(i.t))
        hS(h(p)) = hS(h(p)) - 1
        if (hS(h(p)) == 0) h(vS) = v
        h(p) = mH + 1
        hS(h(p)) = hS(h(p)) + 1; q - qL
    }

    def apply(s:Int, t:Int) = {
        vS = s
        vT = t
        var w = 0
        while (h(vS) < v) w = w + aug(vS, Int.MaxValue); w
    }
}
