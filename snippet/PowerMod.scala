object PowerMod {
    def apply(a:Long, b:Long, m:Long, u:Long = 1L):Long = {
        if (b == 0L) return u
        apply(a * a % m, b >>> 1, m, if ((b & 1L) == 0L) u else u * a % m)
    }
}
