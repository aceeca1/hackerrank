object Solution {
    import io.StdIn._
    import util.parsing.combinator._
    import util.parsing.input._

    object IntuitiveParsers extends JavaTokenParsers {
        type L = () => List[BigR]
        type B = () => BigR
        val a = collection.mutable.Map[String, List[BigR]]()
        def numI = wholeNumber ^^ {_.toInt}
        def num = wholeNumber ^^ {n => () => BigR(BigInt(n), 1)}
        def paren = "(" ~> expr <~ ")"
        def partial(f:List[BigR], x:List[B]) = {
            def g(f:List[BigR], x:List[B], s:BigR):List[BigR] = {
                if (x == Nil) return f :+ s
                g(f.tail, x.tail, s + f.head * x.head())
            }; g(f.init, x, f.last)
        }
        def fResult = ident ~ rep("[" ~> expr <~ "]") ^^ {
            case f ~ x => () => partial(a(f), x).head
        }
        def fact:Parser[B] = num | paren | fResult
        def term = chainl1(fact, "[*/]".r ^^ {
            case "*" => (u1:B, u2:B) => () => u1() * u2()
            case "/" => (u1:B, u2:B) => () => u1() / u2()
        })
        def expr:Parser[B] = chainl1(term, "[+-]".r ^^ {
            case "+" => (u1:B, u2:B) => () => u1() + u2()
            case "-" => (u1:B, u2:B) => () => u1() - u2()
        })
        def exprs = rep1sep(expr, ",")
        def fExpr = ident ~ rep("[" ~> expr <~ "]") ^^ {
            case f ~ x => () => partial(a(f), x)
        } | expr ^^ {u => () => List(u)}
        def s0 = (ident <~ "is") ~ (expr <~ ".") ^^ {
            case f ~ b0 => () => a(f) = List(b0())
        }
        def s1 = (ident <~ "is function of") ~
                (numI <~ ":") ~ (exprs <~ ".") ^^ {
            case f ~ n ~ b => () => a(f) = b.map{_()}
        }
        def s2 = (opt("do" ~> "{" ~> expr <~ "}") <~ "assign") ~
                rep1sep((expr <~ "to") ~ ident, "and") <~ "!" ^^ {
            case r ~ s => () => {
                for (_ <- 1 to r.map{_().n.toInt}.getOrElse(1)) {
                    for ((e ~ i) <- s) a(i) = List(e())
                }
            }
        }
        def s3 = "what is" ~> rep1sep(fExpr, "and") <~ "?" ^^ {r => () =>
            for (e <- r) println(e().mkString(", "))
        }
        def stmt = s3 | s2 | s1 | s0 | success(() => Unit)
        def run(s:String) = parseAll(stmt, s).get()
    }

    def main(args:Array[String]) {
        for (i <- io.Source.stdin.getLines)
            IntuitiveParsers.run(i.toLowerCase)
    }

    case class BigR(n:BigInt, d:BigInt = 1) {
        override def toString = if (d == 1) n.toString else n + "/" + d
        def sim = {
            val d0 = if (d > 0) n.gcd(d) else -n.gcd(d)
            BigR(n / d0, d / d0)
        }
        def +(x:BigR) = {
            val d0 = d / d.gcd(x.d) * x.d
            val n0 = d0 / d * n + d0 / x.d * x.n
            if (n0 != 0) BigR(n0, d0).sim else BigR(0, 1)
        }
        def -(x:BigR) = {
            val d0 = d / d.gcd(x.d) * x.d
            val n0 = d0 / d * n - d0 / x.d * x.n
            if (n0 != 0) BigR(n0, d0).sim else BigR(0, 1)
        }
        def *(x:BigR) = {
            val d0 = n.gcd(x.d)
            val d1 = x.n.gcd(d)
            BigR((n / d0) * (x.n / d1), (d / d1) * (x.d / d0))
        }
        def /(x:BigR) = {
            val d0 = if (x.n > 0) n.gcd(x.n) else -n.gcd(x.n)
            val d1 = x.d.gcd(d)
            BigR((n / d0) * (x.d / d1), (d / d1) * (x.n / d0))
        }
    }
}
