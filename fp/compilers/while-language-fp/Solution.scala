object Solution {
    import io.StdIn._
    import util.parsing.combinator._
    import util.parsing.input._

    object WhileParsers extends JavaTokenParsers {
        type L = () => Long
        type B = () => Boolean
        type U = () => Unit
        val a = collection.mutable.Map[String, Long]()
        val num = wholeNumber^^{n => () => n.toLong}
        val vv = ident^^(v => () => a(v))
        val asNum:Parser[L] = num | vv | "(" ~> expr <~ ")"
        val term = chainl1(asNum, "[*/]".r ^^ {
            case "*" => (u1:L, u2:L) => () => u1() * u2()
            case "/" => (u1:L, u2:L) => () => u1() / u2()
        })
        val expr = chainl1(term, "[+-]".r ^^ {
            case "+" => (u1:L, u2:L) => () => u1() + u2()
            case "-" => (u1:L, u2:L) => () => u1() - u2()
        })
        val bool0 = "true" ^^^ {() => true}
        val bool1 = "false" ^^^ {() => false}
        val bool2 = (expr ~ "[<>]".r ~ expr) ^^ {
            case u1 ~ "<" ~ u2 => () => u1() < u2()
            case u1 ~ ">" ~ u2 => () => u1() > u2()
        }
        val bool:Parser[B] = bool0 | bool1 | bool2 | "(" ~> bExpr <~ ")"
        val bTerm = chainl1(bool, "and" ^^^ {
            (u1:B, u2:B) => () => u1() && u2()
        })
        val bExpr = chainl1(bTerm, "or" ^^^ {
            (u1:B, u2:B) => () => u1() || u2()
        })
        val stmt0 = (ident <~ ":=") ~ expr ^^ {
            case ident ~ expr => () => a(ident) = expr()
        }
        val stmt1 = ("if" ~> bExpr <~ "then" <~ "{") ~
                (stmts <~ "}" <~ "else" <~ "{") ~ (stmts <~ "}") ^^ {
            case b ~ sT ~ sF => () => if (b()) sT() else sF()
        }
        val stmt2 = ("while" ~> bExpr <~ "do" ~ "{") ~ (stmts <~ "}") ^^ {
            case b ~ s => () => while (b()) s()
        }
        val stmt:Parser[U] = stmt0 | stmt1 | stmt2
        val stmts = repsep(stmt, ";") ^^ {u1 => () => u1.foreach(_())}
        def run(s:Reader[Char]) = parseAll(stmts, s).get()
    }

    def main(args:Array[String]) {
        val lines = io.Source.stdin.getLines
        val pq = collection.immutable.PagedSeq.fromLines(lines)
        WhileParsers.run(new PagedSeqReader(pq))
        for ((k, v) <- WhileParsers.a.toBuffer.sorted) {
            print(k); print(' '); println(v)
        }
    }
}
