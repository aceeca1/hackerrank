object Solution {
    import io.StdIn._
    import collection.mutable.ArrayStack
    import collection.mutable.Buffer

    def main(args:Array[String]) {
        val Array(n, m) = readLine.split(' ').map{_.toInt}
        val r = readLine
        val validB = Array.fill(127){false}
        for (i <- "<>+-.,[]") validB(i) = true
        val p = Iterator.fill(m){readLine.filter{validB(_)}}.mkString
        val z = p.size
        val ma = Array.fill(z){0}
        val s = ArrayStack[Int]()
        for (i <- p.indices) p(i) match {
            case '[' => s.push(i)
            case ']' => {ma(i) = s.pop; ma(ma(i)) = i}
            case _ => ()
        }
        var ri = 0
        var bi = 0
        val b = Buffer(0)
        var pi = 0
        var i = 0
        while (true) {
            i = i + 1
            if (pi >= z) {
                println
                return
            }
            if (i > 100000) {
                println
                println("PROCESS TIME OUT. KILLED!!!")
                return
            }
            p(pi) match {
                case '>' => {
                    pi = pi + 1
                    bi = bi + 1
                    if (bi >= b.size) b.+=(0)
                }
                case '<' => {pi = pi + 1; bi = bi - 1}
                case '+' => {pi = pi + 1; b(bi) = (b(bi) + 1) & 0xff}
                case '-' => {pi = pi + 1; b(bi) = (b(bi) - 1) & 0xff}
                case '.' => {pi = pi + 1; print(b(bi).toChar)}
                case ',' => {pi = pi + 1; b(bi) = r(ri); ri = ri + 1}
                case '[' => {pi = if (b(bi) == 0) ma(pi) else pi + 1}
                case ']' => {pi = if (b(bi) != 0) ma(pi) else pi + 1}
            }
        }
    }
}
