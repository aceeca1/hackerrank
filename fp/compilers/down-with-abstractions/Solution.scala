object Solution {
    import io.StdIn._
    import util.parsing.combinator._
    import util.parsing.input._

    case class Apply(f:AnyRef, x:AnyRef) {
        override def toString = x match {
            case _:String => f.toString + x
            case _ => f.toString + "(" + x + ")"
        }
    }
    case class Lambda(f:String, x:AnyRef)

    /* -----Stack Overflow-----
    object LambdaParsers extends JavaTokenParsers {
        val expr0 = ident
        val expr1 = "(" ~> expr ~ expr <~ ")" ^^ {
            case e1 ~ e2 => Apply(e1, e2)
        }
        val expr2 = ("(\\" ~> rep1(ident) <~ ".") ~ (expr <~ ")") ^^ {
            case e1 ~ e2 => e1.foldRight(e2){Lambda(_, _)}
        }
        val expr:Parser[AnyRef] = expr2 | expr1 | expr0
        def apply(s:String) = parseAll(expr, s).get
    } */

    object LambdaParsers {
        import collection.mutable.ArrayStack
        import collection.mutable.StringBuilder
        import collection.mutable.Buffer

        type Ident = StringBuilder
        type Idents = Buffer[String]
        type Expr = AnyRef

        val st = ArrayStack(0)
        val sy = ArrayStack[AnyRef]()

        def pop = {st.pop; sy.pop}
        def popChar = {st.pop; sy.pop.asInstanceOf[Character]}
        def popIdent = {st.pop; sy.pop.asInstanceOf[Ident]}
        def popIdents = {st.pop; sy.pop.asInstanceOf[Idents]}
        def popExpr = {st.pop; sy.pop.asInstanceOf[Expr]}
        def headIdent = {st.pop; sy.head.asInstanceOf[Ident]}
        def headIdents = {st.pop; sy.head.asInstanceOf[Idents]}
        def headExpr = {st.pop; sy.head.asInstanceOf[Expr]}

        val action = Array.fill(127){null:Array[Int]}
        val aa = Array(5, 6, 6, 6, 5, -2, -3, 5, 5, 5)
        for (i <- 'A' to 'z') action(i) = aa
        for (i <- 'a' to 'z') action(i) = aa
        for (i <- '0' to '9') action(i) = aa
        action('_') = aa
        action('(') = Array(4, -1, -1, -1, 4, -1, -1, -1, -1, 4)
        action(')') = Array(8, 9, -1, -6, -1, -2, -3, -1, -7, -8)
        action('\\') = Array(-1, -1, -1, -1, 7, -1, -1, -1, -1, -1)
        action('.') = Array(4, -5, -4, -1, -1, -2, -3, -1, -1, -1)
        action(' ') = Array(8, -5, -4, -6, 9, -2, -3, 0, -7, -8)
        action('$') = Array(-1, -1, -1, -6, -1, -2, -3, -1, -7, -8)
        val gotoIdent = Array(3, -1, -1, -1, 3, -1, -1, 2, 1, 3)
        val gotoIdents = Array(-1, -1, -1, -1, -1, -1, -1, 0, -1, -1)
        val gotoExpr = Array(0, -1, -1, -1, 7, -1, -1, -1, -1, 1)

        var root:AnyRef = null

        val reduce = Array(
            () => Unit,
            () => root = headExpr,
            () => sy.push(new StringBuilder += popChar),
            () => {val p = popChar; headIdent.+=(p)},
            () => sy.push(Buffer(popIdent.toString)),
            () => {val p = popIdent; pop; headIdents.+=(p.toString)},
            () => sy.push(popIdent.toString),
            () => {
                pop; val x = popExpr
                pop; val f = popExpr
                pop; sy.push(Apply(f, x))
            },
            () => {
                pop; val x = popExpr
                pop; pop; val f = popIdents
                pop; pop; sy.push(f.foldRight(x){Lambda(_, _)})
            }
        )

        def apply(s:String) = {
            root = null
            var i = 0
            while (root == null) {
                val si = if (i < s.size) s(i) else '$'
                val a = action(si)(st.head)
                if (a >= 0) {
                    st.push(a)
                    sy.push(new Character(si))
                    i = i + 1
                } else {
                    reduce(-a)()
                    sy.head match {
                        case h:Ident => st.push(gotoIdent(st.head))
                        case h:Idents => st.push(gotoIdents(st.head))
                        case h:Expr => st.push(gotoExpr(st.head))
                    }
                }
            }; root
        }
    }

    def has(e:AnyRef, Str:String):Boolean = e match {
        case Str => true
        case Apply(e0, e1) => has(e0, Str) || has(e1, Str)
        case Lambda(e0, e1) => e0 != Str && has(e1, Str)
        case _ => false
    }

    def toComb(a:AnyRef):AnyRef = a match {
        case Lambda(e0, Apply(e1, e2))
            if e0 == e2 && !has(e1, e0) => toComb(e1)
        case Apply(e0, e1) => Apply(toComb(e0), toComb(e1))
        case Lambda(e0, e1) if e0 == e1 => "I"
        case Lambda(e0, e1) if !has(e1, e0) => Apply("K", toComb(e1))
        case Lambda(e0, Apply(e1, e2)) => {
            val b1 = has(e1, e0)
            val b2 = has(e2, e0)
            if (b1) if (b2) {
                val s0 = Apply("S", toComb(Lambda(e0, e1)))
                Apply(s0, toComb(Lambda(e0, e2)))
            } else {
                val s0 = Apply("C", toComb(Lambda(e0, e1)))
                Apply(s0, toComb(e2))
            } else {
                val s0 = Apply("B", toComb(e1))
                Apply(s0, toComb(Lambda(e0, e2)))
            }
        }
        case Lambda(e0, e) => toComb(Lambda(e0, toComb(e)))
        case a:String => a
    }

    def main(args:Array[String]) {
        for (_ <- 1 to readInt) {
            println(toComb(LambdaParsers(readLine)))
        }
    }
}
