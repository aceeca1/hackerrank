object Solution {
    import io.StdIn._

    def main(args:Array[String]) {
        val n = readInt
        f(n)
    }

    def f(num:Int) = {
        for (i <- 1 to num) println("Hello World")
    }
}
