object Solution {
    import io.StdIn._

    def main(args:Array[String]) {
        val n = readInt
        println(f(n).size)
    }

    def f(num:Int):List[Int] = {
        List.fill(num){0}
    }
}
