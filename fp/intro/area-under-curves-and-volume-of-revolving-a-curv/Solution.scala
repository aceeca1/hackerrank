object Solution {
    import io.StdIn._

    def main(args:Array[String]) {
        val a = readLine.split(' ').map{_.toInt}.toList
        val b = readLine.split(' ').map{_.toInt}.toList
        val Array(l, r) = readLine.split(' ').map{_.toInt}
        println(summation(f, r, l, a, b).formatted("%.1f"))
        println(summation(area, r, l, a, b).formatted("%.1f"))
    }

    def f(a:List[Int], b:List[Int], x:Double):Double = {
        (a, b).zipped.map{case (ai, bi) => ai * math.pow(x, bi)}.sum
    }

    def area(a:List[Int], b:List[Int], x:Double):Double = {
        val c = f(a, b, x)
        math.Pi * c * c
    }

    def summation(
        fun:(List[Int], List[Int], Double) => Double,
        r:Int, l:Int, a:List[Int], b:List[Int]
    ):Double = {
        val lD = l.toDouble
        val rD = r.toDouble
        val it = Iterator.iterate(lD){_ + 0.001}.takeWhile{_ < rD}
        it.map{fun(a, b, _)}.sum * 0.001
    }
}
