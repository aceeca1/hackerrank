object Solution {
    import io.StdIn._

    def main(args:Array[String]) {
        val a = Iterator.continually{readLine}.takeWhile{_ != null}
        val b = a.map{_.toInt}.toList
        println(f(b))
    }

    def f(arr:List[Int]):Int = {
        arr.filter{x => (x & 1) == 1}.sum
    }
}
