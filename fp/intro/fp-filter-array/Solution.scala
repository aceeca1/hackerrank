object Solution {
    import io.StdIn._

    def main(args:Array[String]) {
        val n = readInt
        val a = Iterator.continually{readLine}.takeWhile{_ != null}
        val b = a.map{_.toInt}.toList
        for (i <- f(n, b)) println(i)
    }

    def f(delim:Int, arr:List[Int]):List[Int] = {
        for (i <- arr; if (i < delim)) yield i
    }
}
