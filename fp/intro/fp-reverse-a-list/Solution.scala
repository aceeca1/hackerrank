object Solution {
    import io.StdIn._

    def main(args:Array[String]) {
        val a = Iterator.continually{readLine}.takeWhile{_ != null}
        val b = a.map{_.toInt}.toList
        for (i <- f(b)) println(i)
    }

    def f(arr:List[Int]):List[Int] = {
        arr.reverse
    }
}
