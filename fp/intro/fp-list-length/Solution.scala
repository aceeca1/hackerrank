object Solution {
    import io.StdIn._

    def main(args:Array[String]) {
        val a = Iterator.continually{readLine}.takeWhile{_ != null}
        val b = a.map{_.toInt}.toList
        println(f(b))
    }

    def f(arr:List[Int]):Int = {
        arr.map{x => 1}.sum
    }
}
