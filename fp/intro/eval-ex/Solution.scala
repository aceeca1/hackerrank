object Solution {
    import io.StdIn._

    def main(args:Array[String]) {
        for (i <- 1 to readInt) {
            val x = readFloat
            println(f(x).formatted("%.4f"))
        }
    }

    def f(x:Double):Double = {
        Iterator.iterate((1, 1.0)){
            case (i, s) => (i + 1, s * x / i)
        }.map{_._2}.take(10).sum
    }
}
