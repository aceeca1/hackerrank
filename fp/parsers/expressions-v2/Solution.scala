object Solution {
    import io.StdIn._
    import util.parsing.combinator._

    val M = 1000000007

    object ExpParsers extends JavaTokenParsers {
        type L = Long
        type F = (L, L) => L
        type FL = (F, L)
        val num = wholeNumber ^^ {x => x.toLong}
        val factor = "[ +-]*".r ~ (num | paren) ^^ {case a1 ~ a2 => {
            val v = a1.count{_ == '-'} & 1L
            ((a2 ^ -v) + v + M) % M
        }}
        def add(u:L, v:L) = (u + v) % M
        def sub(u:L, v:L) = (M - v + u) % M
        def mul(u:L, v:L) = u * v % M
        def div(u:L, v:L) = Bezout.divideBy(u, v, M)
        def comb(w:F, u:L, v:FL) = (w, v._1(u, v._2))
        def get1(u:L, v:L) = u
        val term = chainr1(factor, "[*/]".r ^^ {
            case "*" => comb(mul, (_:L), (_:FL))
            case "/" => comb(div, (_:L), (_:FL))
        }, comb(get1, (_:L), (_:FL)), (get1(_, _), 0L)) ^^ {_._2}
        val expr = chainr1(term, "[+-]".r ^^ {
            case "+" => comb(add, (_:L), (_:FL))
            case "-" => comb(sub, (_:L), (_:FL))
        }, comb(get1, (_:L), (_:FL)), (get1(_, _), 0L)) ^^ {_._2}
        val paren:Parser[L] = "(" ~> expr <~ ")"
        def apply(s:String) = parseAll(expr, s).get
    }

    def main(args:Array[String]) {
        println(ExpParsers(readLine))
    }

    object Bezout {
        def divideBy(u:Long, v:Long, m:Long, a:Long = 1L, b:Long = 0L):Long = {
            if (v == 0L) return (u / m * b % a + Math.abs(a)) % a
            val k = m / v
            divideBy(u, m - v * k, v, b - a * k, a)
        }
    }
}
