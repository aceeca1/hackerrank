object Solution {
    import io.StdIn._
    import util.parsing.combinator._
    import collection.mutable.Buffer

    val n = readInt

    object RegParsers extends JavaTokenParsers {
        type M = PartialFunction[Int, String]
        val letter:Parser[M] = "[a-z]".r ^^ {s => {case 1 => s}}
        val term = (letter | paren) ~ opt("*") ^^ {
            case v ~ Some(_) => kleene(v)
            case v ~ None => v
        }
        val terms = rep(term) ^^ {concat(_)}
        val expr = rep1sep(terms, "|") ^^ {orTerm(_)}
        val paren:Parser[M] = "(" ~> expr <~ ")"
        def memoize(m:M) = new M{
            val NIL = "NIL"
            val TODO = "TODO"
            val a = Array.fill(n + 1){null:String}
            def isDefinedAt(x:Int) = a(x) match {
                case NIL => false
                case null => {
                    val b = m.isDefinedAt(x)
                    a(x) = if (b) TODO else NIL; b
                }
                case _ => true
            }
            def apply(x:Int) = a(x) match {
                case NIL => null
                case TODO => {a(x) = m.apply(x); a(x)}
                case ax => ax
            }
        }
        def kleene(m:M) = {var r:M = null; r = memoize(new M{
            def isDefinedAt(x:Int):Boolean = {
                if (x == 0) return true
                for (i <- 1 to x) if (
                    m.isDefinedAt(i) && r.isDefinedAt(x - i)
                ) return true; false
            }
            def apply(x:Int):String = {
                for (i <- 1 to x) if (
                    m.isDefinedAt(i) && r.isDefinedAt(x - i)
                ) return m(i) + r(x - i); ""
            }
        }); r}
        def concat(m:List[M]) = memoize(new M{
            def isDefinedAt(x:Int):Boolean = {
                def f(x:Int, mi:List[M]):Boolean = {
                    if (mi == Nil) return x == 0
                    for (i <- 0 to x) if (
                        mi.head.isDefinedAt(i) && f(x - i, mi.tail)
                    ) return true; false
                }; f(x, m)
            }
            def apply(x:Int):String = {
                def f(x:Int, mi:List[M]):String = {
                    if (mi == Nil) return if (x == 0) "" else null
                    for (i <- 0 to x) if (mi.head.isDefinedAt(i)) {
                        val v = f(x - i, mi.tail)
                        if (v != null) return mi.head(i) + v
                    }; null
                }; f(x, m)
            }
        })
        def orTerm(m:List[M]) = memoize(m.reduceLeft{_.orElse(_)})
        def apply(s:String) = parseAll(expr, s).get
    }

    def main(args:Array[String]) {
        val r = RegParsers(readLine)
        if (!r.isDefinedAt(n)) println("NIL")
        else println(r(n))
    }
}
