object Solution {
    import io.StdIn._
    import collection.mutable.Buffer

    val Array(n, q) = readLine.split(' ').map{_.toInt}
    val p = Array.fill(n + 1){0}
    val c = Array.fill(n + 1){Buffer[Int]()}
    val b = Array.fill(n + 1){SegTree.nulTree}
    for (_ <- 1.until(n)) {
        val Array(u, pa) = readLine.split(' ').map{_.toInt}
        c(pa).+=(u)
        p(u) = pa
    }
    val r = readLine.split(' ').map{_.toInt}
    SegTree.setM(r.max)
    val w = c.map{_.size}
    val av = Buffer[Int]()
    for (i <- 1 to n) if (c(i).size == 0) w(p(i)) = w(p(i)) - 1
    for (i <- 1 to n) if (c(i).size != 0 && w(i) == 0) av.+=(i)
    while (av.size > 0) {
        val avL = av(av.size - 1)
        av.remove(av.size - 1)
        val tree = new SegTree
        for (i <- c(avL)) tree.push(r(i - 1))
        b(avL) = c(avL).foldLeft(tree){(x, s) => x.merge(b(s))}
        w(p(avL)) = w(p(avL)) - 1
        if (w(p(avL)) == 0) av.+=(p(avL))
    }
    val a = Array.range(0, n).sortBy(r)
    def query(v:Int, k:Int) = {
        val s = b(v).select(k)
        a(BinarySearch(0, a.size){i => r(a(i)) >= s}) + 1
    }
    def main(args:Array[String]) {
        var d = 0
        for (_ <- 1 to q) {
            val Array(v, k) = readLine.split(' ').map{_.toInt}
            d = query(v + d, k)
            println(d)
        }
    }

    object SegTree {
        case class Node(var v:Int, var l:Node, var r:Node)
        var m = -1
        def setM(mv:Int) {m = Math.getExponent(mv)}
        val nulNode = Node(0, null, null)
        val nulTree = SegTree(SegTree.nulNode)
        def newNode = Node(0, nulNode, nulNode)
    }
    case class SegTree(root:SegTree.Node = SegTree.newNode) {
        import SegTree._
        def push(n:Int, d:Int = 1, h:Int = m, a:Node = root) {
            a.v = a.v + d
            if (h == -1) return
            if (((n >>> h) & 1) == 0) {
                if (a.l eq nulNode) a.l = newNode
                push(n, d, h - 1, a.l)
            } else {
                if (a.r eq nulNode) a.r = newNode
                push(n, d, h - 1, a.r)
            }
        }
        def select(k:Int, n:Int = 0, h:Int = m, a:Node = root):Int = {
            if (h == -1 || a == nulNode) return n
            if (k <= a.l.v) select(k, n, h - 1, a.l)
            else select(k - a.l.v, n + (1 << h), h - 1, a.r)
        }
        def merge(b:SegTree) = {
            def m(n1:Node, n2:Node):Node = {
                if (n1 == nulNode) return n2
                if (n2 == nulNode) return n1
                Node(n1.v + n2.v, m(n1.l, n2.l), m(n1.r, n2.r))
            }; new SegTree(m(root, b.root))
        }
    }

    object BinarySearch {
        def apply(x1:Int, x2:Int)(f:Int => Boolean):Int = {
            if (x1 == x2) return x1
            val m = (x1 + x2) >>> 1
            if (f(m)) apply(x1, m)(f) else apply(m + 1, x2)(f)
        }
    }
}
