object Solution {
    import io.StdIn._
    import collection.mutable.Buffer

    val n = readInt
    val e = Array.fill(n){readInt}
    val w = Array.fill(n + 1){0}; w(n) = 1
    val p = Array.fill(n){Buffer[Int]()}
    for (i <- e.indices) if (e(i) < 0) p(i + e(i)).+=(i)

    SegTree.setM(e.max + 1)
    val tree = new SegTree

    def compute(i:Int) {
        if (i == -1) return
        if (w(i) == 0) {
            if (e(i) > 0) tree.push(e(i))
            w(i) = tree.median
        }
        if (p(i).size > 0) {
            val pL = p(i)(p(i).size - 1)
            p(i).remove(p(i).size - 1)
            compute(pL)
        } else if (i + 1 < n && e(i + 1) > 0 && w(i + 1) == 0) compute(i + 1)
        else if (e(i) > 0) {tree.push(e(i), -1); compute(i - 1)}
        else compute(i + e(i))
    }; compute(0)

    def main(args:Array[String]) {
        for (i <- 0.until(n)) println(w(i))
    }

    object SegTree {
        case class Node(var v:Int, var l:Node, var r:Node)
        var m = -1
        def setM(mv:Int) {m = Math.getExponent(mv)}
        val nulNode = Node(0, null, null)
        def newNode = Node(0, nulNode, nulNode)
    }
    case class SegTree(root:SegTree.Node = SegTree.newNode) {
        import SegTree._
        def push(n:Int, d:Int = 1, h:Int = m, a:Node = root) {
            a.v = a.v + d
            if (h == -1) return
            if (((n >>> h) & 1) == 0) {
                if (a.l eq nulNode) a.l = newNode
                push(n, d, h - 1, a.l)
            } else {
                if (a.r eq nulNode) a.r = newNode
                push(n, d, h - 1, a.r)
            }
        }
        def select(k:Int, n:Int = 0, h:Int = m, a:Node = root):Int = {
            if (h == -1 || a == nulNode) return n
            if (k <= a.l.v) select(k, n, h - 1, a.l)
            else select(k - a.l.v, n + (1 << h), h - 1, a.r)
        }
        def median = select(root.v + 1 >>> 1)
    }
}
