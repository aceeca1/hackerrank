object Solution {
    import io.StdIn._

    val M = 1000000007
    val Pr = Prime(100)
    val p = Pr.From(2).takeWhile{_ <= 100}.toBuffer
    val pR = Array.fill(101){0}; for (i <- p.indices) pR(p(i)) = i
    val q = Array.tabulate(101){
        case 0 => null
        case i => Pr.factorize(i).map(pR)
    }
    val n = readInt

    object SegTree {
        val m = 1 << (Math.getExponent(n.toFloat) + 1)
        val a = Array.fill[Int](m + m, p.size){0}; {
            val r = readLine.split(' ').map{_.toInt}
            for (i <- r.indices) f1(i + m + 1, r(i))
            for (i <- Range(m - 1, 0, -1)) f2(a(i), a(i + i), a(i + i + 1))
        }
        def f1(i:Int, v:Int) = q(v).foreach{k => a(i)(k) = a(i)(k) + 1}
        def f2(b:Array[Int], b1:Array[Int], b2:Array[Int]) {
            for (i <- b.indices) b(i) = b1(i).max(b2(i))
        }
        def query(l:Int, r:Int) {
            val b = Array.fill(p.size){0}
            def f(u:Int, v:Int) {
                if ((u ^ v) == 1) return
                if ((u & 1) == 0) f2(b, b, a(u + 1))
                if ((v & 1) == 1) f2(b, b, a(v - 1))
                f(u >>> 1, v >>> 1)
            }; f(l + m - 1, r + m + 1)
            val c = Iterator.tabulate(b.size){i => PowerMod(p(i), b(i), M)}
            println(c.reduce(_ * _ % M))
        }
        def update(i:Int, v:Int) {
            def f(u:Int) {
                if (u == 1) return
                f2(a(u), a(u + u), a(u + u + 1))
                f(u >>> 1)
            }; f1(i + m, v); f(i + m >>> 1)
        }
    }; SegTree

    def main(args:Array[String]) {
        val reQ = "Q ([0-9]+) ([0-9]+)".r
        val reU = "U ([0-9]+) ([0-9]+)".r
        for (_ <- 1 to readInt) readLine match {
            case reQ(l, r) => SegTree.query(l.toInt + 1, r.toInt + 1)
            case reU(i, v) => SegTree.update(i.toInt + 1, v.toInt)
        }
    }

    case class Prime(m:Int) {
        val p = Array.fill(m + 1){0}
        private def test(i:Int, u:Int) {
            def sieve(s:Int, t:Int) {
                if (i * s > m) return
                p(i * s) = s
                if (s < t) sieve(p(s), t)
            }
            if (i < m) p(i) match {
                case 0 => {p(u) = i; sieve(2, i); test(i + 1, i)}
                case v => {sieve(2, v); test(i + 1, u)}
            } else p(u) = Int.MaxValue
        }; test(2, 0)
        def apply(n:Int) = p(n) >= n
        def firstFrom(n:Int):Int = if (apply(n)) n else firstFrom(n + 1)
        case class From(n:Int) extends collection.IterableView[Int, From] {
            def iterator = Iterator.iterate(firstFrom(n))(p)
            def underlying = null
        }
        def minFactor(n:Int) = p(n).min(n)
        type B = collection.mutable.Buffer[Int]
        def B() = collection.mutable.Buffer[Int]()
        def factorize(n:Int, b:B = B):B = {
            if (n == 1) return b
            val a = minFactor(n)
            factorize(n / a, b.+=(a))
        }
    }

    object PowerMod {
        def apply(a:Long, b:Long, m:Long, u:Long = 1L):Long = {
            if (b == 0L) return u
            apply(a * a % m, b >>> 1, m, if ((b & 1L) == 0L) u else u * a % m)
        }
    }
}
