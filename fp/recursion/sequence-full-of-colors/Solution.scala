object Solution {
    import io.StdIn._

    def f(s:String):Boolean = {
        var rG = 0
        var yB = 0
        for (i <- s) {
            i match {
                case 'R' => rG = rG + 1
                case 'G' => rG = rG - 1
                case 'Y' => yB = yB + 1
                case 'B' => yB = yB - 1
            }
            if (rG < -1) return false
            if (rG > 1) return false
            if (yB < -1) return false
            if (rG > 1) return false
        }
        return rG == 0 && yB == 0
    }

    def main(args:Array[String]) {
        for (t <- 1 to readInt) {
            val s = readLine
            println(if (f(s)) "True" else "False")
        }
    }
}
