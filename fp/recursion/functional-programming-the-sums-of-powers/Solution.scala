object Solution {
    import io.StdIn._
    import collection.mutable.Buffer

    val x = readInt
    val n = readInt
    val a = Array.fill(x + 1, x + 1){0}
    val tN = Iterator.from(0).map{x =>
        Iterator.fill(n){x}.product
    }.takeWhile{_ <= x}.toBuffer

    def w(k:Int, u:Int):Int = () match {
        case _ if k == 0 => 1
        case _ if u >= tN.size => 0
        case _ => Iterator.range(u, tN.size).takeWhile{tN(_) <= k}.map{i => {
            w(k - tN(i), i + 1)
        }}.sum
    }

    def main(args:Array[String]) {
        println(w(x, 1))
    }
}
