object Solution {
    import io.StdIn._
    import math._

    def main(args:Array[String]) {
        val s5 = sqrt(5.0)
        val a = 0.5 * (s5 + 1.0)
        val n = readDouble - 1.0
        println(round(pow(a, n) / s5))
    }
}
