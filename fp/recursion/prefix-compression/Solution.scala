object Solution {
    import io.StdIn._

    def main(args:Array[String]) {
        val s1 = readLine
        val s2 = readLine
        val m = s1.size.min(s2.size)
        var p = 0
        while (p < m && s1(p) == s2(p)) p = p + 1
        print(p)
        print(' ')
        println(s1.take(p))
        print(s1.size - p)
        print(' ')
        println(s1.drop(p))
        print(s2.size - p)
        print(' ')
        println(s2.drop(p))
    }
}
