// v-a-r and v-a-l have been blocked in this problem.
object Solution {
    import io.StdIn._

    def aaLinePrint(x:Array[Int]) = println(x.map{
        case 0 => '_'
        case 1 => '1'
    }.mkString)

    def nextLine(f:(Int, Int, Int) => Int)(
        a:Array[Int]
    ) = Array.tabulate(a.size){
        case i if i == 0          => f(0, a(i), a(i + 1))
        case i if i == a.size - 1 => f(a(i - 1), a(i), 0)
        case i => f(a(i - 1), a(i), a(i + 1))
    }

    def main(args:Array[String]) {
        val n = readInt
        Iterator.iterate(Array.tabulate(63){case 31 => 1; case _ => 0}){b => {
            (for {
                _ <- Iterator(0)
                a = Iterator.iterate(b)(nextLine{_ | _ | _})
                _ = a.take(31 >>> n).foreach(aaLinePrint)
                c = a.next
                _ = aaLinePrint(c)
            } yield {
                nextLine{(x1, x2, x3) => (x1 ^ x3) & ~x2}(c)
            }).next
        }}.take((1 << n) + 1).foreach{x => ()}
    }
}
