object Solution {
    import io.StdIn._

    def main(args:Array[String]) {
        for (t <- 1 to readInt) {
            val s = readLine
            for (i <- 0.until(s.size, 2)) {
                print(s(i + 1))
                print(s(i))
            }
            println
        }
    }
}
