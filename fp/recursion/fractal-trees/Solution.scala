// v-a-r and v-a-l have been blocked in this problem.
object Solution {
    import io.StdIn._

    def aaLinePrint(a:Array[Int]) {
        (a.toIterator ++ Iterator(100)).buffered match {
            case b => for (i <- 0.until(100)) {
                if (i < b.head) print('_') else {
                    while (i == b.head) b.next
                    print('1')
                }
            }
        }
        println
    }

    def main(args:Array[String]) {
        (for {
            _ <- Iterator(0)
            n = readInt
            _ = for (i <- 1.to((1 << 6 - n) - 1)) println("_" * 100)
            h = 17 + (1 << (5 - n))
            a = Array.range(h, 99 - h, 1 << (6 - n))
        } yield (for {
            i1 <- 0.until(n)
            i2 <- 0.to(1)
            i3 <- 1.to(1 << (i1 + 5 - n))
        } {
            aaLinePrint(a)
            if (i2 == 0) for (i4 <- a.indices) {
                a(i4) = a(i4) + 1 - (((i4 >> i1) & 1) << 1)
            }
        })).next
    }
}
