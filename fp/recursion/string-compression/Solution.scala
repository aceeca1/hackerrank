object Solution {
    import io.StdIn._

    def main(args:Array[String]) {
        val s = readLine
        def f(k:Int, c:Char, n:Int) {
            if (k < s.size && s(k) == c) {
                f(k + 1, c, n + 1)
            } else {
                print(c)
                if (n > 1) print(n)
                if (k < s.size) f(k + 1, s(k), 1)
            }
        }
        f(1, s.head, 1)
        println
    }
}
