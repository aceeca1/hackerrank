object Solution {
    import io.StdIn._

    def main(args:Array[String]) {
        val s1 = readLine
        val s2 = readLine
        for (i <- s1.indices) {
            print(s1(i))
            print(s2(i))
        }
        println
    }
}
