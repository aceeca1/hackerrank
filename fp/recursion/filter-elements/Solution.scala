object Solution {
    import io.StdIn._

    def main(args:Array[String]) {
        for (t <- 1 to readInt) {
            val Array(n, k) = readLine.split(' ').map{_.toInt}
            val a = readLine.split(' ').map{_.toInt}
            val b = a.indices.groupBy(a).toIterator
            val bF = b.filter{_._2.size >= k}.toBuffer
            val c = bF.sortBy{_._2.head}.map{_._1}
            println(if (c.isEmpty) "-1" else c.mkString(" "))
        }
    }
}
