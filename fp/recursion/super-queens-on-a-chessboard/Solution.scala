object Solution {
    import io.StdIn._

    val n = readInt
    val a = Array.fill(n){0}

    def wPlace(k:Int):Int = {
        if (k == n) return 1
        Iterator.range(0, n).filter{i => 1.to(k).forall{iA => {
            ((if (iA < 3) 31 << i >> 2 else
                ((((1 << iA) + 1) << iA) + 1) << i >> iA
            ) & a(k - iA)) == 0
        }}}.map{i => {
            a(k) = 1 << i
            wPlace(k + 1)
        }}.sum
    }

    def main(args:Array[String]) {
        println(wPlace(0))
    }
}
