object Solution {
    import io.StdIn._

    def main(args:Array[String]) {
        val Array(a1, a2) = readLine.split(' ').map{_.toInt}
        println(BigInt(a1).gcd(BigInt(a2)))
    }
}
