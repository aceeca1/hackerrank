object Solution {
    import io.StdIn._

    case class Point(c1:Int, c2:Int) {
        def -(x:Point) = Point(c1 - x.c1, c2 - x.c2)
        def cross(x:Point) = c1 * x.c2 - c2 * x.c1
        def <(x:Point) = c1 < x.c1 || c1 == x.c1 && c2 < x.c2
        def abs = Math.sqrt(c1 * c1 + c2 * c2)
    }

    def cross3(p1:Point, p2:Point, p3:Point) = {
        (p2 - p1).cross(p3 - p2)
    }

    def main(args:Array[String]) {
        val n = readInt
        val a = Array.fill(n){
            val Array(x1, x2) = readLine.split(' ').map{_.toInt}
            Point(x1, x2)
        }.sortWith{_ < _}
        val u = a.indexWhere{cross3(a(0), a(1), _) != 0}
        object Dq {
            val m = (1 << (Math.getExponent(n + 2) + 1)) - 1
            val b = Array.ofDim[Point](m + 1); {
                val v = u - 1
                if (cross3(a(0), a(v), a(u)) > 0) {
                    b(0) = a(u); b(1) = a(0); b(2) = a(v); b(3) = a(u)
                } else {
                    b(0) = a(u); b(1) = a(v); b(2) = a(0); b(3) = a(u)
                }
            }
            var p = 0
            var q = 3
            def h1 = b(p)
            def h2 = b((p + 1) & m)
            def t1 = b(q)
            def t2 = b((q - 1) & m)
            def popH = {p = (p + 1) & m}
            def popT = {q = (q - 1) & m}
            def pushD(x:Point) = {
                p = (p - 1) & m
                b(p) = x
                q = (q + 1) & m
                b(q) = x
            }
            def empty = {p - q == 1}
        }
        for (i <- a.toIterator.drop(u + 1)) {
            var k = false
            while (cross3(i, Dq.h1, Dq.h2) <= 0) {
                k = true
                Dq.popH
            }
            while (cross3(Dq.t2, Dq.t1, i) <= 0) {
                k = true
                Dq.popT
            }
            if (k) Dq.pushD(i)
        }
        def f(s:Double, p:Point):Double = {
            if (Dq.empty) s else {
                val h = Dq.h1
                Dq.popH
                f(s + (h - p).abs, h)
            }
        }
        val h = Dq.h1
        Dq.popH
        println(f(0.0, h).formatted("%.1f"))
    }
}
