object Solution {
    import io.StdIn._
    import util.parsing.combinator._

    case class Node(var v:Int, c0:Node, c1:Node)
    val dot = Node(0, null, null)

    object TreeParser extends JavaTokenParsers {
        def leafC = "X|\\.".r.^^{x => if (x.head == 'X') 1 else 0}
        def leafN = leafC.^^{x => Node(x, dot, dot)}
        def expr:Parser[Node] = leafN | ("(" ~> expr ~ leafC ~ expr <~ ")").^^{
            case c0 ~ v ~ c1 => Node(v, c0, c1)
        }
        def apply(s:String) = parseAll(expr, s).get
    }

    val n = readInt

    def updateTree(c:Node, p:Int) {
        val v = c.v
        c.v = (n >> ((p << 3) + (c.c0.v << 2) + (c.v << 1) + c.c1.v)) & 1
        if (c.c0 != dot) updateTree(c.c0, v)
        if (c.c1 != dot) updateTree(c.c1, v)
    }

    def main(args:Array[String]) {
        val tree = TreeParser(readLine)
        var u = 0
        val a = Array.fill(readInt){
            val re = "([^ ]+) \\[([<>]*)\\]".r
            val re(s, p) = readLine
            u = u + s.toInt
            (u, p)
        }
        val c = Array.fill(a.size){'-'}
        var s = 0
        for (i <- Array.range(0, a.size).sortBy{a(_)._1}) {
            val (u, p) = a(i)
            while (s < u) {
                s = s + 1
                updateTree(tree, 0)
            }
            var q = tree
            for (i <- p) if (i == '<') q = q.c0 else q = q.c1
            c(i) = if (q.v == 1) 'X' else '.'
        }
        for (i <- c) println(i)
    }
}
