object Solution {
    import io.StdIn._

    def main(args:Array[String]) {
        val x = readLine.split(' ').map{_.map{_.toInt - '0'}.sum}.product
        println((x - 1) % 9 + 1)
    }
}
