// v-a-r and v-a-l have been blocked in this problem.
object Solution {
    import io.StdIn._

    def main(args:Array[String]) {
        for (a <- Iterator.iterate(Array(1)){a => Array.tabulate(a.size + 1){
            case i if i == 0      => 1
            case i if i == a.size => 1
            case i => a(i) + a(i - 1)
        }}.take(readInt)) println(a.mkString(" "))
    }
}
