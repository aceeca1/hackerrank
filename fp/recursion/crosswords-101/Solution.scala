object Solution {
    import io.StdIn._
    import collection.mutable.Buffer

    val a = Array.fill(10){readLine.map{
        case '-' => 3
        case '+' => 0
    }.toArray}

    object Wd {
        val w = readLine.split(';')
        var p = w.size
        def wSwap(x1:Int, x2:Int) {
            val t = w(x1)
            w(x1) = w(x2)
            w(x2) = t
        }
    }

    object Bl {
        case class Blank(c1:Int, c2:Int, d:Int, len:Int)
        val b = Buffer[Blank]()
        for (i1 <- 0 until 10; i2 <- 0 until 10) {
            if ((a(i1)(i2) & 1) > 0) {
                a(i1)(i2) = a(i1)(i2) ^ 1
                var p1 = i1 + 1
                while (p1 < 10 && ((a(p1)(i2) & 1) > 0)) {
                    a(p1)(i2) = a(p1)(i2) ^ 1
                    p1 = p1 + 1
                }
                if (p1 > i1 + 1) {
                    b += Blank(i1, i2, 1, p1 - i1)
                }
            }
            if ((a(i1)(i2) & 2) > 0) {
                a(i1)(i2) = a(i1)(i2) ^ 2
                var p2 = i2 + 1
                while (p2 < 10 && ((a(i1)(p2) & 2) > 0)) {
                    a(i1)(p2) = a(i1)(p2) ^ 2
                    p2 = p2 + 1
                }
                if (p2 > i2 + 1) {
                    b += Blank(i1, i2, 2, p2 - i2)
                }
            }
        }
        var p = b.size
    }

    val e = Array.fill(10, 10){'+'}
    val r = Array.fill(10, 10){0}

    def fill:Boolean = {
        if (Bl.p == 0) return true
        val b = Bl.b(Bl.p - 1)
        def fillOne(w:String):Boolean = {
            if (w.size != b.len) return false
            var p = 0
            if (b.d == 1) for (i <- b.c1.until(b.c1 + b.len)) {
                if (r(i)(b.c2) > 0 && e(i)(b.c2) != w(p)) {
                    for (i0 <- b.c1.until(i)) {
                        r(i0)(b.c2) = r(i0)(b.c2) - 1
                    }
                    return false
                }
                r(i)(b.c2) = r(i)(b.c2) + 1
                e(i)(b.c2) = w(p)
                p = p + 1
            } else for (i <- b.c2.until(b.c2 + b.len)) {
                if (r(b.c1)(i) > 0 && e(b.c1)(i) != w(p)) {
                    for (i0 <- b.c2.until(i)) {
                        r(b.c1)(i0) = r(b.c1)(i0) - 1
                    }
                    return false
                }
                r(b.c1)(i) = r(b.c1)(i) + 1
                e(b.c1)(i) = w(p)
                p = p + 1
            }
            return true
        }
        def unfillOne {
            if (b.d == 1) for (i <- b.c1.until(b.c1 + b.len)) {
                r(i)(b.c2) = r(i)(b.c2) - 1
            } else for (i <- b.c2.until(b.c2 + b.len)) {
                r(b.c1)(i) = r(b.c1)(i) - 1
            }
        }
        for (i <- 0.until(Wd.p); if (fillOne(Wd.w(i)))) {
            Bl.p = Bl.p - 1
            Wd.wSwap(i, Wd.p - 1)
            Wd.p = Wd.p - 1
            if (fill) return true
            Wd.p = Wd.p + 1
            Wd.wSwap(i, Wd.p - 1)
            Bl.p = Bl.p + 1
            unfillOne
        }
        return false
    }

    def main(args:Array[String]) {
        fill
        for (i <- e) println(i.mkString)
    }
}
