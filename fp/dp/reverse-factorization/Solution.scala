object Solution {
    import io.StdIn._
    import collection.mutable.Buffer

    val Array(n, m) = readLine.split(' ').map{_.toInt}
    val a = readLine.split(' ').map{_.toLong}.sorted

    var b = Buffer[Long](1)
    var maxD = Int.MaxValue
    var bestB:Buffer[Long] = null

    def put(i:Int, d:Int) {
        if (d >= maxD) return
        for (k <- i until m) {
            val k0 = b.last * a(k)
            if (k0 > n) return
            if (k0 == n) {
                maxD = d
                bestB = b.clone
                return
            }
            b.+=(k0)
            put(k, d + 1)
            b.remove(b.size - 1)
        }
    }

    def main(args:Array[String]) {
        put(0, 0)
        if (bestB == null) {
            println("-1")
        } else {
            println(bestB.+=(n).mkString(" "))
        }
    }
}
