object Solution {
    import io.StdIn._
    import collection.mutable.Buffer

    val n = readInt
    val a = readLine.split(' ').map{_.toInt}

    def main(args:Array[String]) {
        val b = Array.fill(n, 101){(0:Char, 0)}
        b(0)(a(0)) = ('=', 0)
        for (i <- 0 to n - 2; j <- 0 to 100; if (b(i)(j)._1 != 0)) {
            b(i + 1)((j + a(i + 1)) % 101) = ('+', j)
            b(i + 1)((j + 101 - a(i + 1)) % 101) = ('-', j)
            b(i + 1)((j * a(i + 1)) % 101) = ('*', j)
        }
        val u = Buffer[String]()
        def f(v:Int, w:Int) {
            val (op, l) = b(v)(w)
            u.+=(a(v).toString).+=(op.toString)
            if (v > 1) f(v - 1, l)
        }; f(n - 1, 0)
        print(a(0))
        for (i <- u.reverseIterator) print(i)
        println
    }
}
