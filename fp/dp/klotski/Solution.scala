object Solution {
    import io.StdIn._
    import collection.mutable.Buffer

    val dT = Array((0, 1), (0, -1), (1, 0), (-1, 0))
    val Array(n, m) = readLine.split(' ').map{_.toInt}
    val a = Array.fill(n){readLine.split(' ')}
    for (i <- 0 until n; j <- 0 until m)
        if (a(i)(j).forall{_ == '.'}) a(i)(j) = "."
    val b = readLine
    val Array(tN, tM) = readLine.split(' ').map{_.toInt}
    var e = Buffer[String]()
    for (i1 <- a; i2 <- i1) yield e.+=(i2)
    e = e.sorted.distinct.-=(".")

    case class S(a:Array[Array[String]], c:String, from:S) {
        def find(b:String) = {
            var bN = Int.MaxValue
            var bM = Int.MaxValue
            for (ni <- 0 until n; mi <- 0 until m; if (a(ni)(mi) == b)) {
                bN = bN.min(ni)
                bM = bM.min(mi)
            }; (bN, bM)
        }
        def reach = find(b) == (tN, tM)
        def go(i:String, dti:Int):S = {
            val aa = Array.fill(n, m){"."}
            for (jn <- 0 until n; jm <- 0 until m; if (a(jn)(jm) != ".")) {
                val kn = if (a(jn)(jm) == i) jn + dT(dti)._1 else jn
                val km = if (a(jn)(jm) == i) jm + dT(dti)._2 else jm
                if (kn < 0 || n <= kn) return null
                if (km < 0 || m <= km) return null
                if (aa(kn)(km) != ".") return null
                aa(kn)(km) = a(jn)(jm)
            }; S(aa, i, this)
        }
        override def hashCode = {
            import util.hashing.MurmurHash3._
            arrayHash(a.map{arrayHash(_)}) ^ stringHash(c)
        }
        override def toString = {
            a.map{_.mkString(" ")}.mkString("\n") + "\n" + c + "\n"
        }
    }

    val s = new java.util.ArrayDeque[S]
    s.addLast(S(a, ".", null))
    val visited = collection.mutable.HashSet[Int]()

    def solve:S = {while (true) {
        var sH = s.removeFirst
        while (visited(sH.hashCode)) sH = s.removeFirst
        visited.+=(sH.hashCode)
        for (i <- e; dti <- dT.indices) {
            val r = sH.go(i, dti)
            if (r != null && !visited(r.hashCode)) {
                if (r.reach) return r
                if (sH.c == r.c) s.addFirst(r)
                else s.addLast(r)
            }
        }
    }; null}

    def main(args:Array[String]) {
        if (s.getFirst.reach) {println(0);return}
        val bu = Buffer[String]()
        def trace(s:S, c:String) {
            if (s.c != c) {
                bu.+=(s.find(c).toString)
                bu.+=(c)
                bu.+=(s.find(s.c).toString)
            }
            if (s.c != ".") trace(s.from, s.c)
        }; trace(solve, " ")
        println(bu.size / 3 - 1)
        for (i <- Range(bu.size - 2, 1, -3)) {
            print(bu(i)); print(' ')
            print(bu(i - 1)); print(' ')
            println(bu(i - 2))
        }
    }
}
