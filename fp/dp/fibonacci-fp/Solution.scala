object Solution {
    import io.StdIn._

    def main(args:Array[String]) {
        val n = readInt
        val a = Array.fill(n){readInt}
        val m = a.max
        val fib = Array.fill(m + 1){0}
        fib(1) = 1
        for (i <- 2 until fib.size) {
            fib(i) = (fib(i - 1) + fib(i - 2)) % 100000007
        }
        for (i <- a) println(fib(i))
    }
}
