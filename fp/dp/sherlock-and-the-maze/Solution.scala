object Solution {
    import io.StdIn._

    val M = 1000000007

    def main(args:Array[String]) {
        val a = Array.fill(readInt){readLine.split(' ').map{_.toInt}}
        val n = a.map{_(0)}.max.max(a.map{_(1)}.max)
        val k = a.map{_(2)}.max
        val b = Array.fill(n + 1, n + 1, k + 1){0}
        b(1)(1) = Array.fill(k + 1){1}
        for {
            i <- 3 to n + n
            i1 <- (i - n).max(1) to (i - 1).min(n)
            i2 = i - i1
            ki <- 0 to k
            b1 = b(i1 - 1)(i2)(ki)
        } b(i1)(i2)(ki) = if (ki == 0 || i == 3) b1 else {
            (b1 + b(i2)(i1 - 1)(ki - 1)) % M
        }
        for (Array(n, m, k) <- a) {
            if (n == 1 && m == 1) println(1)
            else println((b(n)(m)(k) + b(m)(n)(k)) % M)
        }
    }
}
