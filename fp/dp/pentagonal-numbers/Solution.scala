object Solution {
    import io.StdIn._

    def main(args:Array[String]) {
        for (_ <- 1 to readInt) {
            val n = readLong
            println((3 * n * n - n) >>> 1)
        }
    }
}
