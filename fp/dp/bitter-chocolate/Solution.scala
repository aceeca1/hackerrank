object Solution {
    import io.StdIn._

    def main(args:Array[String]) {
        for (_ <- 1 to readInt) {
            val Array(a1, a2, a3) = readLine.split(' ').map{_.toInt}
            val a = Array.fill(a1 + 1, a2 + 1, a3 + 1){false}
            for (i1 <- 1 to a1; i2 <- 0 to a2.min(i1); i3 <- 0 to a3.min(i2)) {
                for (i <- 1 until i1; j1 = i; j2 = i2.min(i); j3 = i3.min(i))
                    if (a(j1)(j2)(j3) == false) a(i1)(i2)(i3) = true
                for (i <- 0 until i2; j1 = i1; j2 = i; j3 = i3.min(i))
                    if (a(j1)(j2)(j3) == false) a(i1)(i2)(i3) = true
                for (i <- 0 until i3; j1 = i1; j2 = i2; j3 = i)
                    if (a(j1)(j2)(j3) == false) a(i1)(i2)(i3) = true
            }
            println(if (a(a1)(a2)(a3)) "WIN" else "LOSE")
        }
    }
}
