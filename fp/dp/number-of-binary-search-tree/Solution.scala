object Solution {
    import io.StdIn._

    def main(args:Array[String]) {
        for (_ <- 1 to readInt) {
            val n = readInt
            var c = BigInt(1)
            var m = n + n
            var k = 1
            while (k <= n) {
                c = c * m / k
                m = m - 1
                k = k + 1
            }
            println(c / (n + 1) % 100000007)
        }
    }
}
