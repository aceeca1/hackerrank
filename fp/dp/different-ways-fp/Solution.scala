object Solution {
    import io.StdIn._

    def main(args:Array[String]) {
        for (_ <- 1 to readInt) {
            val Array(n, k) = readLine.split(' ').map{_.toInt}
            var c = BigInt(1)
            var i = 1
            var m = n
            while (i <= k) {
                c = c * m / i
                m = m - 1
                i = i + 1
            }
            println(c % 100000007)
        }
    }
}
