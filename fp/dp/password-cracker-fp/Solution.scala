object Solution {
    import io.StdIn._

    def main(args:Array[String]) {
        for (_ <- 1 to readInt) {
            val n = readInt
            val a = readLine.replaceAll(" ", "|").r
            val s = readLine
            val b = a.findAllIn(s).toBuffer
            if (b.map{_.size}.sum != s.size) println("WRONG PASSWORD")
            else println(b.mkString(" "))
        }
    }
}
