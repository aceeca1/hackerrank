object Solution {
    import io.StdIn._

    val n = readInt
    val a = readLine.split(' ').map{_.toInt match {
        case 0 => 9
        case k => k - 1
    }}

    def main(args:Array[String]) {
        val m = Array.fill(n + 1, 10, 10){0x3fffffff}
        m(n) = Array.fill(10, 10){0}
        for (i <- Range(n - 1, -1, -1); j <- 0 to 9) {
            for (k <- 0 to 9) {
                val v1 = m(i + 1)(j)(k) + Math.abs(a(i) - k) + 1
                m(i)(j)(a(i)) = m(i)(j)(a(i)).min(v1)
                val v2 = m(i + 1)(k)(j) + Math.abs(a(i) - k) + 1
                m(i)(a(i))(j) = m(i)(a(i))(j).min(v2)
            }
        }
        println(m(0).map{_.min}.min)
    }
}
