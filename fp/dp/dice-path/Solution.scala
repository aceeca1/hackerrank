object Solution {
    import io.StdIn._

    val rotR = Array(
        7,  11, 12, 16, 3,  10, 13, 20, 2,  6,  17, 21,
        22, 18, 5,  1,  23, 14, 9,  0,  19, 15, 8,  4)
    val rotD = Array(
        9,  18, 6,  13, 15, 1,  21, 11, 4,  23, 3,  16,
        7,  20, 0,  19, 12, 2,  22, 8,  10, 17, 5,  14)

    def main(args:Array[String]) {
        val q = Array.fill(readInt){readLine.split(' ').map{_.toInt}}
        val m = q.view.map{_(0)}.max
        val n = q.view.map{_(1)}.max
        val a = Array.fill(m + 2, n + 2, 24){Int.MinValue}
        a(1)(1)(1) = 1
        for (i1 <- 1 to m; i2 <- 1 to n; i3 <- 0 to 23) {
            val k1 = a(i1)(i2)(i3) + (rotD(i3) >> 2) + 1
            a(i1 + 1)(i2)(rotD(i3)) = a(i1 + 1)(i2)(rotD(i3)).max(k1)
            val k2 = a(i1)(i2)(i3) + (rotR(i3) >> 2) + 1
            a(i1)(i2 + 1)(rotR(i3)) = a(i1)(i2 + 1)(rotR(i3)).max(k2)
        }
        for (i <- q) println(a(i(0))(i(1)).max)
    }
}
