object Solution {
    import io.StdIn._
    import collection.mutable.Buffer

    def main(args:Array[String]) {
        for (_ <- 1 to readInt) {
            val s = readLine
            val n = s.toInt
            println(if (!Prime.is(n) || s.indexOf('0') != -1) "DEAD" else {
                val l = s.tails.take{s.size}.forall{i => Prime.is(i.toInt)}
                val r = s.inits.take{s.size}.forall{i => Prime.is(i.toInt)}
                if (l) {if (r) "CENTRAL" else "LEFT"}
                else {if (r) "RIGHT" else "DEAD"}
            })
        }
    }

    object Prime {
        val p = Array.fill(65536){0}; {
            def g(u:Int)(i:Int) {
                def f(v:Int)(w:Int) {
                    if (i * w >= 65536) return;
                    p(i * w) = w
                    if (w < v) f(v)(p(w))
                }
                if (i < 65536) p(i) match {
                    case 0 => {p(u) = i; f(i)(2); g(i)(i + 1)}
                    case v => {f(v)(2); g(u)(i + 1)}
                } else p(u) = 65536
            }; g(0)(2)
        }

        def is(n:Int):Boolean = {
            if (n < 65536) return p(n) > n
            def f(i:Int):Boolean = {
                if (i * i > n) return true
                if (n % i == 0) return false
                f(p(i))
            }; f(2)
        }
    }
}
