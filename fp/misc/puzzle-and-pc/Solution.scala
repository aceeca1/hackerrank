object Solution {
    import io.StdIn._

    def place(r0:Int, c0:Int, rb:Int, cb:Int, n:Int) {
        val rm = Array(r0 + n - 1, r0 + n - 1, r0 + n, r0 + n)
        val cm = Array(c0 + n - 1, c0 + n, c0 + n - 1, c0 + n)
        val rs = Array(r0, r0, r0 + n, r0 + n)
        val cs = Array(c0, c0 + n, c0, c0 + n)
        val k = (if (rb >= r0 + n) 2 else 0) + (if (cb >= c0 + n) 1 else 0)
        println((for {
            i <- 0 to 3
            if (i != k)
            m <- Array(rm, cm)
        } yield m(i) + 1).mkString(" "))
        rm(k) = rb
        cm(k) = cb
        if (n == 1) return
        for (i <- 0 to 3) place(rs(i), cs(i), rm(i), cm(i), n >> 1)
    }

    def main(args:Array[String]) {
        val n = 1 << (readInt - 1)
        val Array(rb, cb) = readLine.split(' ').map{_.toInt - 1}
        place(0, 0, rb, cb, n)
    }
}
