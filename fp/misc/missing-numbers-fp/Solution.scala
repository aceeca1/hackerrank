object Solution {
    import io.StdIn._

    def main(args:Array[String]) {
        val a = Array.fill(2){
            readLine
            val v = readLine.split(' ').map{_.toInt}
            v.groupBy{identity}.mapValues{_.size}.withDefaultValue(0)
        }
        println(a(1).keys.filter{k => {
            a(1)(k) - a(0)(k) > 0
        }}.toBuffer.sorted.mkString(" "))
    }
}
