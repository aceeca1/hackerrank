object Solution {
    import io.StdIn._
    import collection.mutable.Buffer
    import util.parsing.combinator._

    var id = 0

    case class Node(s:String, b:Buffer[Node]) {
        def pick0:Node = {
            if (s != null && s(0) == '#') Node('#' + s + '#' + id, null)
            else if (b == null) this else Node(s, b.map(_.pick0))
        }
        def pick = {id = id + 1; pick0}
        def plug(m:Values):Node = {
            if (s != null && s(0) == '#') m(s).plug(m)
            else if (b == null) this else Node(s, b.map(_.plug(m)))
        }
        override def toString = s match {
            case "%r" => "[" + b.head + ": " + b.tail.mkString(", ") + "]"
            case _ => s
        }
    }

    object WatLogParsers extends JavaTokenParsers {
        def name = "[A-Za-z][A-Za-z0-9-]*".r^^{Node(_, null)}
        def variable = "#[A-Za-z][A-Za-z0-9-]*".r^^{Node(_, null)}
        def relTerm0 = ("[" ~> name ~ (":" ~> rep1sep(term, ",")) <~ "]")
        def relTerm = relTerm0^^{case a1 ~ a2 =>
            Node("%r", Buffer(a1).++=(a2))
        }
        def term:Parser[Node] = name | variable | relTerm
        def eqAssert = ("<" ~> term ~ ("=" ~> term) <~ ">")^^{case a1 ~ a2 =>
            Node("%e", Buffer(a1, a2))
        }
        def neqAssert = ("<" ~> term ~ ("/=" ~> term) <~ ">")^^{case a1 ~ a2 =>
            Node("%n", Buffer(a1, a2))
        }
        def cTerm = term | eqAssert | neqAssert
        def rule = (term <~ ".")^^{a1 =>
            Node("%u", Buffer(Node(null, Buffer[Node]()), a1))
        } | (
            "{(" ~> repsep(cTerm, ",") ~ (") =>" ~> term <~ "}.")
        )^^{case a1 ~ a2 =>
            Node("%u", Buffer(Node(null, a1.reverse.toBuffer), a2))
        }
        def query = ("(" ~> repsep(cTerm, ",") <~ ")?")^^{a1 =>
            Node("%q", a1.toBuffer)
        }
        def cmd = "quit!"^^^{Node("%i", null)}
        def noOp = "(%.*)?".r^^^{null:Node}
        val op = rule | query | cmd | noOp
        def apply(s:String) = parseAll(op, s).get
    }

    type Values = Map[String, Node]

    case class Assoc(var m:Values) {
        def ancestor(n:Node):Node = {
            if (n.s(0) != '#' || !m.contains(n.s)) return n
            val a = ancestor(m(n.s))
            m = m.+(n.s -> a); a
        }

        def e(t1:Node, t2:Node):Values = {
            for (i <- m("%#").b) {
                if (t1 == i.b(0) && t2 == i.b(1)) return null
                if (t1 == i.b(1) && t2 == i.b(0)) return null
            }
            val a1 = ancestor(t1)
            val a2 = ancestor(t2)
            if (a1 eq a2) return m
            if (a1.s == "%r" && a2.s == "%r") {
                if (a1.b.size != a2.b.size) return null
                for (i <- a1.b.indices) {
                    m = e(a1.b(i), a2.b(i))
                    if (m == null) return null
                }
                return m
            }
            if (a1.s == a2.s) return m
            if (a1.s(0) == '#') return m.+(a1.s -> a2)
            if (a2.s(0) == '#') return m.+(a2.s -> a1)
            return null
        }

        def ne(t1:Node, t2:Node):Values = {
            val a1 = ancestor(t1)
            val a2 = ancestor(t2)
            if (a1 eq a2) return null
            if (a1.s == "%r" && a2.s == "%r") return m
            if (a1.s == a2.s) null else m
        }
    }

    val kBase = Buffer[Node]()
    var sat = false

    def check(m:Values) {
        val n = m.filter{_._1(1) != '#'}.mapValues(_.plug(m))
        for (i <- m("%#").b) if (i.b(0).plug(m) == i.b(1).plug(m)) return
        sat = true
        if (n.size == 0) {println("SAT"); return}
        println("SAT:")
        println("=====")
        val nB = n.toBuffer
        for ((k, v) <- nB.sortBy{_._1}) {
            print(k)
            print(" := ")
            println(v)
        }
    }

    def prove(b:Buffer[Node], m:Values) {
        if (m == null) return
        val z = b.size
        if (z == 0) return check(m)
        val bT = b(z - 1)
        b.remove(z - 1)
        bT.s match {
            case "%e" => prove(b, Assoc(m).e(bT.b(0), bT.b(1)))
            case "%n" => {
                val mb = m("%#").b
                mb.+=(bT)
                prove(b, Assoc(m).ne(bT.b(0), bT.b(1)))
                mb.remove(mb.size - 1)
            }
            case _ => for (ki <- kBase; i = ki.pick) {
                prove(b.++=(i.b(0).b), Assoc(m).e(bT, i.b(1)))
                while (b.size >= z) b.remove(b.size - 1)
            }
        }
        b.+=(bT)
    }

    def parseLine:Boolean = {
        val n = WatLogParsers(readLine)
        if (n == null) return true
        if (n.s == "%i") {
            println("Bye.")
            return false
        }
        if (n.s == "%u") {
            kBase.+=(n)
            println("Ok.")
            return true
        }
        sat = false
        prove(n.b.reverse, Map("%#" -> Node(null, Buffer[Node]())))
        if (!sat) println("UNSAT")
        println("Ready."); true
    }

    def main(args:Array[String]) {
        while (parseLine) {}
    }
}
