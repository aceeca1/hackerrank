object Solution {
    import io.StdIn._

    def main(args:Array[String]) {
        println(Iterator.fill(2){
            readLine
            readLine.split(' ').map{BigInt(_)}.product
        }.reduce(_ gcd _) % 1000000007)
    }
}
