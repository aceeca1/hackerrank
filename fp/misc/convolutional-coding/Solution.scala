object Solution {
    import io.StdIn._
    import collection.mutable.Buffer

    class ConvCode {
        val Array(n, k) = readLine.split(' ').map{_.toInt}
        val a = Array.fill(n){Integer.parseInt(readLine, 2)}

        def encodePrint(s:Buffer[Char]) {
            s.++=("0" * k)
            s.foldLeft(0){(p, q) => {
                val e = (q - '0' << k - 1) + (p >>> 1)
                for (ai <- a) {
                    val c = '0' + (Integer.bitCount(ai & e) & 1)
                    print(c.toChar)
                }; e
            }}
            println
        }

        def decode(s:Buffer[Char]) = {
            val z = s.size / n
            val b = Array.fill(z + 1, 1 << k){(0, 0)}
            b(z) = Array.fill(1 << k){(0x3fffffff, 0)}
            b(z)(0) = (0, 0)
            for (i <- Range(z - 1, 0, -1); j <- 0.until(1 << k)) {
                val ev = for {
                    e <- if (i > z - k) Iterator(j >>> 1)
                    else {Iterator(j >>> 1, (j >>> 1) + (1 << k - 1))}
                    u = Iterator.from(i * n)
                    v = a.map{ai =>
                        s(u.next) - '0' ^ (Integer.bitCount(ai & e) & 1)
                    }.sum + b(i + 1)(e)._1
                } yield (v, e)
                b(i)(j) = ev.minBy(_._1)
            }
            val r = Buffer[Char]()
            def f(i:Int, v:Int) {
                if (i > z - k) return
                val c = (v >> k - 1) + '0'
                r.+=(c.toChar)
                f(i + 1, b(i)(v)._2)
            }
            f(1, b(0)(0)._2); r
        }
    }

    def main(args:Array[String]) {
        val c1 = new ConvCode
        val c2 = new ConvCode
        val s = Buffer[Char]()
        while (true) {
            val r = readLine
            if (r == null) {
                c2.encodePrint(c1.decode(s))
                return
            }
            s.++=(r.filter{i => i == '0' || i == '1'})
        }
    }
}
