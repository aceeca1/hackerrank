object Solution {
    import io.StdIn._

    def main(args:Array[String]) {
        for (t <- 1 to readInt) {
            val s = readLine
            for (i <- 1 to s.size) {
                val (s1, s2) = s.splitAt(i)
                print(s2)
                print(s1)
                if (i != s.size) print(' ')
            }
            println
        }
    }
}
