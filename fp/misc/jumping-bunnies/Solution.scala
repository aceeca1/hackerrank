object Solution {
    import io.StdIn._

    def main(args:Array[String]) {
        readLine
        println(readLine.split(' ').map{BigInt(_)}.reduce{
            (x1, x2) => x1 / x1.gcd(x2) * x2
        })
    }
}
