object Solution {
    import io.StdIn._

    def main(args:Array[String]) {
        val n = readInt
        val a = readLine.split(' ').map{_.toLong}.sortWith{_ > _}
        val b = a.scanLeft(0L){_ + _}
        for (_ <- 1 to readInt) {
            val s = readLong
            println(BinarySearch(1, b.size){b(_) >= s} match {
                case n if n == b.size => -1
                case n => n
            })
        }
    }

    object BinarySearch {
        def apply(x1:Int, x2:Int)(f:Int => Boolean):Int = {
            if (x1 == x2) return x1
            val m = (x1 + x2) >>> 1
            if (f(m)) apply(x1, m)(f) else apply(m + 1, x2)(f)
        }
    }
}
