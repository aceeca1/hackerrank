object Solution {
    import io.StdIn._

    def main(args:Array[String]) {
        val Array(nS, mS) = readLine.split(' ')
        val n = nS.toInt
        val m = mS.toLong
        val a = readLine.split(' ').map{_.toLong}
        val h = readLine.split(' ').map{_.toLong}
        println(BinarySearch(1, n + 1){k => {
            val b = Array.tabulate(n){i => a(i) + (k - 1) * h(i)}.sorted
            b.toIterator.take(k).sum > m
        }} - 1)
    }

    object BinarySearch {
        def apply(x1:Int, x2:Int)(f:Int => Boolean):Int = {
            if (x1 == x2) return x1
            val m = (x1 + x2) >>> 1
            if (f(m)) apply(x1, m)(f) else apply(m + 1, x2)(f)
        }
    }
}

