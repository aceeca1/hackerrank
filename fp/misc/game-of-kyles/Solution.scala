object Solution {
    import io.StdIn._

    object SG {
        val m = 82 // = 70 (non-repeating part) + 12 (repeating part)
        val v = Array.fill(m){0}
        v(1) = 1
        for (i <- 2 until m) {
            val b = Array.fill(m){false}
            for (u <- Array(i - 1, i - 2); k <- 0.to(u >>> 1)) {
                b(v(k) ^ v(u - k)) = true
            }
            v(i) = b.indexOf(false)
        }

        def apply(n:Int) = if (n < m) v(n) else v((n - 70) % 12 + 70)
    }

    def main(args:Array[String]) {
        for (_ <- 1 to readInt) {
            readLine
            val a = readLine.split("X+").view
            val b = a.map{i => SG(i.size)}.reduce{_ ^ _}
            println(if (b > 0) "WIN" else "LOSE")
        }
    }
}
