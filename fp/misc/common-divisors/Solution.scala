object Solution {
    import io.StdIn._
    import Math._

    def main(args:Array[String]) {
        for (t <- 1 to readInt) {
            val Array(l, m) = readLine.split(' ').map{_.toInt}
            val g = BigInt(l).gcd(m).toInt
            val s = floor(sqrt(g)).toInt
            val a = 1.to(s).count{g % _ == 0}
            println(if (s * s == g) a + a - 1 else a + a)
        }
    }
}
