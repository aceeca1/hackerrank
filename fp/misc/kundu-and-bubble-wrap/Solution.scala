object Solution {
    import io.StdIn._

    def main(args:Array[String]) {
        val x = readLine.split(' ').map{_.toInt}.product
        val xD = x.toDouble
        println(Iterator.range(x, 0, -1).map{xD / _}.sum)
    }
}
