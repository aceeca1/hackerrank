// Acknowledgement: http://t.cn/Rw28yfx
object Solution {
    import io.StdIn._

    val Array(n, k) = readLine.split(' ').map{_.toInt}
    val a = readLine.split(' ').map{_.toLong}

    case class Node(
        l:Long, lt:Int, r:Long, rt:Int,
        b:Long, bt1:Int, bt2:Int, s:Long
    )

    object SegTree {
        val m = 1 << Math.getExponent(n + 2) + 1
        val nulNode = Node(-1L, 1, -1L, 1, -1L, 1, 1, -1L)
        val b = Array.fill(m + m){null:Node}
        for (i <- m until b.size) b(i) = nulNode
        for (i <- a.indices) {
            val i1 = i + 1
            b(m + i1) = Node(a(i), i1, a(i), i1, a(i), i1, i1, a(i))
        }
        def comb(a1:Node, a2:Node):Node = {
            var l = a1.s + a2.l
            var lt = a2.lt
            if (a1.l >= l) {l = a1.l; lt = a1.lt}
            var r = a2.s + a1.r
            var rt = a1.rt
            if (a2.r > r) {r = a2.r; rt = a2.rt}
            var b = a1.r + a2.l
            var bt1 = a1.rt
            var bt2 = a2.lt
            if (a1.b > b || a1.b == b && a1.bt1 <= bt1)
                {b = a1.b; bt1 = a1.bt1; bt2 = a1.bt2}
            if (a2.b > b) {b = a2.b; bt1 = a2.bt1; bt2 = a2.bt2}
            var s = a1.s + a2.s
            Node(l, lt, r, rt, b, bt1, bt2, s)
        }
        def fix(i:Int) {
            if (nulNode.eq(b(i))) return
            b(i) = comb(b(i + i), b(i + i + 1))
        }
        for (i <- Range(m - 1, 0, -1)) fix(i)
        def pop = {
            val b1 = b(1)
            var p = m + b1.bt1 - 1
            var q = m + b1.bt2 + 1
            while ((p ^ q) != 1) {
                if ((p & 1) == 0) b(p ^ 1) = nulNode
                if ((q & 1) == 1) b(q ^ 1) = nulNode
                p = p >>> 1; fix(p)
                q = q >>> 1; fix(q)
            }
            while (p > 1) {
                p = p >>> 1; fix(p)
            }; b1.b
        }
    }

    def main(args:Array[String]) {
        for (i <- 1 to k) {
            val p = SegTree.pop
            if (p < 0) return
            println(p)
        }
    }
}
