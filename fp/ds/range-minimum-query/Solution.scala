object Solution {
    import io.StdIn._

    val Array(n, m) = readLine.split(' ').map{_.toInt}
    val st = new java.util.StringTokenizer(readLine)
    val a = Array.fill(n){st.nextToken.toInt}

    object SegTree {
        val m = 1 << Math.getExponent(n + 1) + 1
        val b = Array.fill(m + m){Int.MaxValue}
        a.copyToArray(b, m + 1)
        def fix(i:Int) = b(i) = b(i + i).min(b(i + i + 1))
        for (i <- Range(m - 1, 0, -1)) fix(i)
        def query(u:Int, v:Int) = {
            var p = u + m - 1
            var q = v + m + 1
            var x = Int.MaxValue
            while ((p ^ q) != 1) {
                if ((p & 1) == 0) x = x.min(b(p + 1))
                if ((q & 1) == 1) x = x.min(b(q - 1))
                p = p >>> 1; q = q >>> 1
            }; x
        }
    }

    def main(args:Array[String]) {
        for (i <- 1 to m) {
            val Array(u, v) = readLine.split(' ').map{_.toInt}
            println(SegTree.query(u + 1, v + 1))
        }
    }
}
