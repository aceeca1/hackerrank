object Solution {
    import io.StdIn._

    val Array(n, m, r) = readLine.split(' ').map{_.toInt}
    val a = Array.fill(n){readLine.split(' ').map{_.toInt}}
    val b = Array.fill(n, m){0}
    val nMid = n + 1 >>> 1
    val mMid = m + 1 >>> 1

    def toLV(x:Int, y:Int):(Int, Int) = {
        val lx = if (x >= nMid) n - 1 - x else x
        val ly = if (y >= mMid) m - 1 - y else y
        val l = lx.min(ly)
        if (y == l) return (l, x - l)
        val s1 = n - 1 - l - l
        if (x == n - 1 - l) return (l, s1 + y - l)
        val s2 = s1 + m - 1 - l - l
        if (y == m - 1 - l) return (l, s2 + n - 1 - x - l)
        val s3 = s2 + n - 1 - l - l
        return (l, s3 + m - 1 - y - l)
    }

    def toXY(l:Int, v:Int):(Int, Int) = {
        val c = (n + m - (l << 2) - 2) << 1
        val vc = v % c
        val s1 = n - 1 - l - l
        if (vc <= s1) return (vc + l, l)
        val s2 = s1 + m - 1 - l - l
        if (vc <= s2) return (n - 1 - l, vc + l - s1)
        val s3 = s2 + n - 1 - l - l
        if (vc <= s3) return (s2 + n - 1 - vc - l, m - 1 - l)
        return (l, s3 + m - 1 - vc - l)
    }

    def newPlace(x:Int, y:Int) = {
        val (l, v) = toLV(x, y)
        toXY(l, v + r)
    }

    def main(args:Array[String]) {
        for (i <- 0 until n; j <- 0 until m) {
            val (i0, j0) = newPlace(i, j)
            b(i0)(j0) = a(i)(j)
        }
        for (i <- b) println(i.mkString(" "))
    }
}
