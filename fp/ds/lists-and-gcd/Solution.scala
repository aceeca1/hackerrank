object Solution {
    import io.StdIn._

    def main(args:Array[String]) {
        val a = Iterator.fill(readInt){
            readLine.split(' ').map{_.toInt}.grouped(2).map{
                case Array(p, r) => p -> r
            }.toMap
        }.reduce{(x1, x2) => {
            for ((k, v) <- x1; if (x2.contains(k))) yield (k, v.min(x2(k)))
        }}
        var sp = false
        for ((k, v) <- a.toBuffer.sorted) {
            if (sp) print(' '); sp = true
            print(k); print(' '); print(v);
        }
        println
    }
}
