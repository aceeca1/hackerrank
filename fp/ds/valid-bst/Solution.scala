object Solution {
    import io.StdIn._
    import collection.SeqView

    def test(a:SeqView[Int, Array[Int]]):Boolean = {
        if (a.size == 0) return true
        val (a0, a1) = a.tail.span{_ < a.head}
        a1.forall{_ > a.head} && test(a0) && test(a1)
    }

    def main(args:Array[String]) {
        for (_ <- 1 to readInt) {
            val n = readInt
            val a = readLine.split(' ').map{_.toInt}
            println(if (test(a.view)) "YES" else "NO")
        }
    }
}
