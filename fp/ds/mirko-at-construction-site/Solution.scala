// Acknowledgement: http://t.cn/Rw4vKIc
object Solution {
    import io.StdIn._
    import collection.mutable.Buffer

    val Array(n, q) = readLine.split(' ').map{_.toInt}
    val st1 = new java.util.StringTokenizer(readLine)
    val st2 = new java.util.StringTokenizer(readLine)

    case class Point(c1:Long, c2:Long) {
        def -(x:Point) = Point(c1 - x.c1, c2 - x.c2)
        def cross(x:Point) = c1 * x.c2 - c2 * x.c1
        def <(x:Point) = c1 < x.c1 || c1 == x.c1 && c2 < x.c2
        def abs = Math.sqrt(c1 * c1 + c2 * c2)
    }

    val p = Array.fill(n){
        Point(st2.nextToken.toLong, st1.nextToken.toLong)
    }

    def cross3(p1:Point, p2:Point, p3:Point) = {
        (p2 - p1).cross(p3 - p2)
    }

    def main(args:Array[String]) {
        val pis = p.indices.sortWith{(i1, i2) => p(i1) < p(i2)}
        val b = Buffer(pis(0), pis(1))
        def bT1 = b(b.size - 1)
        def bT2 = b(b.size - 2)
        def bPopT = b.remove(b.size - 1)
        for (si <- 2 until p.size; i = pis(si)) {
            while (b.size > 1 && cross3(p(bT2), p(bT1), p(i)) > 0) bPopT
            b.+=(i)
        }
        val qu = Array.fill(q){readInt}
        val a = Array.fill(q){0L}
        val qis = qu.indices.sortBy(qu)
        var k = 0
        for (i <- qis) {
            def f(k:Int) = p(b(k)).c1 * qu(i) + p(b(k)).c2
            while (k + 1 < b.size && f(k + 1) > f(k)) k = k + 1
            a(i) = b(k)
            var e = k
            while (e + 1 < b.size && f(e + 1) == f(e)) {
                e = e + 1
                a(i) = a(i).max(b(e))
            }
        }
        for (i <- a) println(i + 1)
    }
}
