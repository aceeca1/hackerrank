object Solution {
    import io.StdIn._

    val n = readInt
    val st = new java.util.StringTokenizer(readLine)
    val a = Array.fill(n){st.nextToken.toInt}

    object SegTree {
        val m = 1 << Math.getExponent(n + 1) + 1
        val bMax = Array.fill(m + m){0}
        val bMin = Array.fill(m + m){0}
        a.copyToArray(bMax, m + 1)
        a.copyToArray(bMin, m + 1)
        def fix(i:Int) {
            bMax(i) = bMax(i + i).max(bMax(i + i + 1))
            bMin(i) = bMin(i + i).min(bMin(i + i + 1))
        }
        for (i <- Range(m - 1, 0, -1)) fix(i)
        def go(u:Int, v:Int, w:Int, z:Int) = {
            def valid(k:Int) = bMin(k) >= v && bMax(k) <= w
            var p = u + m + 1
            while (p > 1 && ((p & 1) != z || valid(p ^ 1))) p = p >>> 1
            p = p ^ 1
            while (p < m) {
                p = (p << 1) ^ z
                if (valid(p)) p = p ^ 1
            }; p - m - 1
        }
    }

    def main(args:Array[String]) {
        for (_ <- 1 to readInt) {
            val Array(d, k) = readLine.split(' ').map{_.toInt}
            val go0 = SegTree.go(d, a(d), a(d) + k, 0)
            val go1 = SegTree.go(d, a(d), a(d) + k, 1)
            println(go0 - go1 - 1)
        }
    }
}
