object Solution {
    import io.StdIn._

    val n = readInt
    val l = Array.fill(n + 1){0}
    val r = Array.fill(n + 1){0}
    for (i <- 1 to n) {
        val Array(lc, rc) = readLine.split(' ').map{_.toInt}
        l(i) = lc; r(i) = rc
    }
    val v = Array.fill(n + 1){false}

    var sp = false
    def inOrder(e:Int, d:Int) {
        if (e == -1) return
        if (v(d)) inOrder(r(e), d + 1) else inOrder(l(e), d + 1)
        if (sp) print(' '); sp = true; print(e)
        if (v(d)) inOrder(l(e), d + 1) else inOrder(r(e), d + 1)
    }

    def main(args:Array[String]) {
        for (_ <- 1 to readInt) {
            val k = readInt
            for (i <- Range(k, n, k)) v(i) = !v(i)
            sp = false
            inOrder(1, 1)
            println
        }
    }
}
