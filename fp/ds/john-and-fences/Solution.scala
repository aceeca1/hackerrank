object Solution {
    import io.StdIn._
    import collection.SeqView

    val n = readInt
    val a = readLine.split(' ').map{_.toInt}

    def f(u:Int, v:Int):Int = {
        if (u == v) return a(0)
        var p = u + v >>> 1
        var q = p + 1
        var mv = f(u, p).max(f(q, v))
        var h = a(p).min(a(q))
        while (h > 0) {
            while (p > u && a(p - 1) >= h) p = p - 1;
            while (q < v && a(q + 1) >= h) q = q + 1;
            mv = mv.max((q - p + 1) * h)
            h = h - 1
        }; mv
    }

    def main(args:Array[String]) {
        println(f(0, a.size - 1))
    }
}
