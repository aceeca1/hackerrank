object Solution {
    import io.StdIn._

    def main(args:Array[String]) {
        for (_ <- 1 to readInt) {
            println(if (readLine.contains(readLine)) "YES" else "NO")
        }
    }
}
