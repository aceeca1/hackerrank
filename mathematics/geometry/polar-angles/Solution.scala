object Solution {
    import io.StdIn._
    import math._

    def main(args:Array[String]) {
        val n = readInt
        val a = Array.fill(n){readLine.split(' ').map{_.toInt}}
        val b = a.sortBy{x => (-atan2(x(1), -x(0)), hypot(x(0), x(1)))}
        for (i <- b) {
            println(i.mkString(" "))
        }
    }
}
