object Solution {
    import io.StdIn._

    def main(args:Array[String]) {
        val n = readInt
        val a = readLine.split(' ').map{_.toInt}
        def f(b:Int):Boolean = {
            var v = 0
            for (i <- a) {
                v = v + i
                if (v > b) return false
                if (v == b) v = 0
            }
            return v == 0
        }
        val b = a.scanLeft(0){_ + _}
        val s = b.last
        println(b.tail.filter{x => s % x == 0 && f(x)}.mkString(" "))
    }
}
