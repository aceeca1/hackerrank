object Solution {
    import io.StdIn._
    import math._

    def main(args:Array[String]) {
        val s5 = sqrt(5.0)
        val a = 0.5 * (s5 + 1.0)
        for (i <- 1 to readInt) {
            val n = readDouble
            val g = rint(log(s5 * n) / log(a))
            println((rint(pow(a, g) / s5) == n) match {
                case true => "IsFibo"
                case _ => "IsNotFibo"
            })
        }
    }
}
