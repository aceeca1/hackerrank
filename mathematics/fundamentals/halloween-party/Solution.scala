object Solution {
    import io.StdIn._

    def main(args:Array[String]) {
        for (i <- 1 to readInt) {
            val k = readLong
            val a = k >>> 1
            println(a * (k - a))
        }
    }
}
