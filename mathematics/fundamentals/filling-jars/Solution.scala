object Solution {
    import io.StdIn._

    def main(args:Array[String]) {
        val sc = new java.util.Scanner(System.in)
        val n = sc.next.toInt
        val m = sc.next.toInt
        println(Iterator.fill(m){
            val a = sc.next.toLong
            val b = sc.next.toLong
            val k = sc.next.toLong
            (b - a + 1) * k
        }.sum / n)
    }
}
