object Solution {
    import io.StdIn._

    def main(args:Array[String]) {
        val n = readInt
        val a = readLine.split(' ').map{_.toLong}
        val b = a.scanRight(0L){_ + _}
        val v = b.reduce{_ + _}
        val s = b.head
        println(a.scanLeft(v){_ + _ * n - s}.max)
    }
}
