object Solution {
    import io.StdIn._

    def main(args:Array[String]) {
        val Array(n, m) = readLine.split(' ').map{_.toInt}
        val a = readLine.split(' ').map{_.toLong}
        val b = readLine.split(' ').map{_.toInt}
        val c = readLine.split(' ').map{_.toInt}
        val u = b.max
        val d = Array.fill(u + 1){1L}
        for (i <- 0 until m) d(b(i)) = d(b(i)) * c(i) % 1000000007L
        for {
            i <- 1 to u
            j <- (i - 1).until(n, i)
        } a(j) = a(j) * d(i) % 1000000007L
        println(a.mkString(" "))
    }
}
