object Solution {
    import io.StdIn._

    def main(args:Array[String]) {
        for (i <- 1 to readInt) {
            val Array(n, k) = readLine.split(' ').map{_.toInt}
            println(if (k < (n >>> 1)) k + k + 1 else (n - 1 - k) << 1)
        }
    }
}
