object Solution {
    import io.StdIn._

    def main(args:Array[String]) {
        for (i <- 1 to readInt) {
            val n = readInt
            val a = readLine.split(' ').map{x => BigInt(x.toInt)}
            println(a.reduce{_ gcd _}.toInt match {
                case 1 => "YES"
                case _ => "NO"
            })
        }
    }
}
