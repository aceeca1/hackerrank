object Solution {
    import io.StdIn._

    def main(args:Array[String]) {
        for (i <- 1 to readInt) {
            val Array(l, b) = readLine.split(' ').map{_.toInt}
            val g = BigInt(l).gcd(BigInt(b)).toInt
            println((l / g) * (b / g))
        }
    }
}
