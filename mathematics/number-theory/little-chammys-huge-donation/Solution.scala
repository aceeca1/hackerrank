object Solution {
    import io.StdIn._

    def main(args:Array[String]) {
        for (i <- 1 to readInt) {
            val n = readLong
            var k = Math.pow((n * 3).toDouble, 1.0 / 3.0).toLong
            if (k * (1 + k) * (1 + k + k) / 6 > n) k = k - 1
            println(k)
        }
    }
}
