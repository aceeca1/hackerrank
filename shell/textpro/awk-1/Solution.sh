#!/bin/sh
exec lua -e "$(cat <<'EOF'
for i in io.lines() do
    if not i:match(".+ %d+ %d+ %d+") then
        io.write("Not all scores are available for ", i:match("(.-) "), "\n")
    end
end
EOF
)"
