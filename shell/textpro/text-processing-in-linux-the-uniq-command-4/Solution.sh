#!/bin/sh
exec lua -e "$(cat <<'EOF'
x = io.read("*l")
a = 1
for i in io.lines() do
    if i ~= x then
        if a == 1 then
            io.write(x, "\n")
        end
        x = i
        a = 1
    else
        a = a + 1
    end
end
if a == 1 then
    io.write(x, "\n")
end
EOF
)"
