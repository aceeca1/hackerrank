#!/bin/sh
exec lua -e "$(cat <<'EOF'
for i in io.lines() do
    print((i:gsub("%f[%a]the%f[%A]", "this", 1)))
end
EOF
)"
