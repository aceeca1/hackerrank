#!/bin/sh
exec lua -e "$(cat <<'EOF'
a = {}
for i in io.lines() do
    a[#a + 1] = i
end
for i = math.max(#a - 19, 1), #a do
    print(a[i])
end
EOF
)"
