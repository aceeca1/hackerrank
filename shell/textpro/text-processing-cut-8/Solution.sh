#!/bin/sh
exec lua -e "$(cat <<'EOF'
for i in io.lines() do
    print((i .. "   "):match("^.- .- .- "):match("^.*[^ ]") or "")
end
EOF
)"
