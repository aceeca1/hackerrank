#!/bin/sh
exec lua -e "$(cat <<'EOF'
for i in io.lines() do
    print((i:gsub("%f[%a][tT][hH][yY]%f[%A]", "your")))
end
EOF
)"
