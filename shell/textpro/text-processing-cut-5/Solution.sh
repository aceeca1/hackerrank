#!/bin/sh
exec lua -e "$(cat <<'EOF'
for i in io.lines() do
    print((i .. "\t\t\t"):match("^.-\t.-\t.-\t"):match("^.*[^\t]") or "")
end
EOF
)"
