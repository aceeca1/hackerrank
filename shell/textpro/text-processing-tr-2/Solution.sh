#!/bin/sh
exec lua -e "$(cat <<'EOF'
s = io.read("*a"):gsub("[a-z]", "")
io.write(s)
EOF
)"
