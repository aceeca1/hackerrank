#!/bin/sh
exec lua -e "$(cat <<'EOF'
a = {}
for i in io.lines() do
    a[#a + 1] = i
end
table.sort(a)
for i = #a, 1, -1 do
    print(a[i])
end
EOF
)"
