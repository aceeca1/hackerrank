#!/bin/sh
exec lua -e "$(cat <<'EOF'
s = io.read("*a"):gsub(" +", " ")
io.write(s)
EOF
)"
