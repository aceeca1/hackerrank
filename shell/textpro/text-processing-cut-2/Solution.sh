#!/bin/sh
exec lua -e "$(cat <<'EOF'
for i in io.lines() do
    print(i:sub(2, 2) .. i:sub(7, 7))
end
EOF
)"
