#!/bin/sh
exec lua -e "$(cat <<'EOF'
sp = false
for i in io.lines() do
    if sp then
        io.write(";")
    end
    sp = true
    io.write(i)
end
EOF
)"
