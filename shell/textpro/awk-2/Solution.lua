for i in io.lines() do
    i0, i1, i2, i3 = i:match("(.+) (%d+) (%d+) (%d+)")
    a1, a2, a3 = tonumber(i1), tonumber(i2), tonumber(i3)
    if a1 >= 50 and a2 >= 50 and a3 >= 50 then
        io.write(i0, " : Pass\n")
    else
        io.write(i0, " : Fail\n")
    end
end
