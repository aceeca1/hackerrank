#!/bin/sh
exec lua -e "$(cat <<'EOF'
for i in io.lines() do
    if not i:lower():find("%f[%a]that%f[%A]") then
        print(i)
    end
end
EOF
)"
