#!/bin/sh
exec lua -e "$(cat <<'EOF'
for i in io.lines() do
    iL = i:lower()
    if iL:find("%f[%a]then?%f[%A]") or iL:find("%f[%a]that%f[%A]") or
    iL:find("%f[%a]those%f[%A]") then
        print(i)
    end
end
EOF
)"
