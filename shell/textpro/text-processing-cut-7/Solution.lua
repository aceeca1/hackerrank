for i in io.lines() do
    if i:find(" ") then
        print((i .. "    "):match("^.- .- .- (.-) ") or "")
    else
        print(i)
    end
end
