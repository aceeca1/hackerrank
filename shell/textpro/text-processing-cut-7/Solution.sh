#!/bin/sh
exec lua -e "$(cat <<'EOF'
for i in io.lines() do
    if i:find(" ") then
        print((i .. "    "):match("^.- .- .- (.-) ") or "")
    else
        print(i)
    end
end
EOF
)"
