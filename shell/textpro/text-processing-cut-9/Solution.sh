#!/bin/sh
exec lua -e "$(cat <<'EOF'
for i in io.lines() do
    io.write(i:gsub("^.-\t", "") .. "\n")
end
EOF
)"
