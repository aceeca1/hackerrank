#!/bin/sh
exec lua -e "$(cat <<'EOF'
x = nil
for i in io.lines() do
    if i ~= x then
        print(i)
        x = i
    end
end
EOF
)"
