#!/bin/sh
exec lua -e "$(cat <<'EOF'
a = {}
for i in io.lines() do
    a[#a + 1] = {tonumber(i), i}
end
table.sort(a, function(x1, x2)
    return x1[1] < x2[1]
end)
for i = 1, #a do
    print(a[i][2])
end
EOF
)"
