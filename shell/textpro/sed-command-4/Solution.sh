#!/bin/sh
exec lua -e "$(cat <<'EOF'
for i in io.lines() do
    print((i:gsub("%d", "*", 12)))
end
EOF
)"
