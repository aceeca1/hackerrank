#!/bin/sh
exec lua -e "$(cat <<'EOF'
for i in io.lines() do
    print(i:sub(1, 4))
end
EOF
)"
