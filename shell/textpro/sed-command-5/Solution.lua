for i in io.lines() do
    i1, i2, i3, i4 = i:match("(%d+) (%d+) (%d+) (%d+)")
    if i4 then
        io.write(i4, " ", i3, " ", i2, " ", i1, "\n")
    else
        print(i)
    end
end
