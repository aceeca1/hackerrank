#!/bin/sh
exec lua -e "$(cat <<'EOF'
x = io.read("*l")
a = 1
for i in io.lines() do
    if i ~= x then
        io.write(a, " ", x, "\n")
        x = i
        a = 1
    else
        a = a + 1
    end
end
io.write(a, " ", x, "\n")
EOF
)"
