#!/bin/sh
exec lua -e "$(cat <<'EOF'
for i in io.lines() do
    if i:find("%f[%a]the%f[^%a]") then
        print(i)
    end
end
EOF
)"
