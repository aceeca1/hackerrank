#!/bin/sh
exec lua -e "$(cat <<'EOF'
a = {}
for i in io.lines() do
    a[#a + 1] = {tonumber(i:match("^.-|(.-)|")), #a, i}
end
table.sort(a, function(x1, x2)
    return x1[1] < x2[1] or x1[1] == x2[1] and x1[2] < x2[2]
end)
for i = 1, #a do
    print(a[i][3])
end
EOF
)"
