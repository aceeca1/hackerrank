#!/bin/sh
exec lua -e "$(cat <<'EOF'
x = io.read()
if x == "Y" or x == "y" then
    print("YES")
else
    print("NO")
end
EOF
)"
