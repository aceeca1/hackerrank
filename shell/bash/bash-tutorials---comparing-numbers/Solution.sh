#!/bin/sh
exec lua -e "$(cat <<'EOF'
x = io.read()
y = io.read()
if x < y then
    print("X is less than Y")
elseif x == y then
    print("X is equal to Y")
else
    print("X is greater than Y")
end
EOF
)"
