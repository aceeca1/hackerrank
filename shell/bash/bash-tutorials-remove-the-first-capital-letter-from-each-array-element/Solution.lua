sep = false
for i in io.lines() do
    if sep then
        io.write(' ')
    end
    sep = true
    io.write((i:gsub("^%u", ".", 1)))
end
print()
