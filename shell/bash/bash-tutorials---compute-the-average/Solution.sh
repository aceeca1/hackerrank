#!/bin/sh
exec lua -e "$(cat <<'EOF'
x = io.read()
s = 0
for i = 1, x do
    s = s + io.read()
end
print(string.format("%.3f", s / x))
EOF
)"
