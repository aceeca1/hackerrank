#!/bin/sh
exec lua -e "$(cat <<'EOF'
x = io.read()
y = io.read()
print(x + y)
print(x - y)
print(x * y)
print(math.floor(x / y))
EOF
)"
