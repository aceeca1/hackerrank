#!/bin/sh
exec lua -e "$(cat <<'EOF'
load([[print(string.format("%.3f", ]] .. io.read() .. [[))]])()
EOF
)"
