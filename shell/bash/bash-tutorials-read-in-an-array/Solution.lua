sep = false
for i in io.lines() do
    if sep then
        io.write(' ')
    end
    sep = true
    io.write(i)
end
print()
