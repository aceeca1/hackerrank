#!/bin/sh
exec lua -e "$(cat <<'EOF'
sep = false
no = 0
for i in io.lines() do
    if 3 <= no and no <= 7 then
        if sep then
            io.write(' ')
        end
        sep = true
        io.write(i)
    end
    no = no + 1
end
print()
EOF
)"
