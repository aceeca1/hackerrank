#!/bin/sh
exec lua -e "$(cat <<'EOF'
a = {}
for i in io.lines() do
    a[#a + 1] = i
end
sep = false
for k = 1, 3 do
    for i, v in ipairs(a) do
        if sep then
            io.write(' ')
        end
        sep = true
        io.write(v)
    end
end
print()
EOF
)"
