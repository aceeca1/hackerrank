object Solution {
    import io.StdIn._

    def main(args:Array[String]) {
        for (i <- 1 to readInt) {
            val s = readLine
            val n = s.size
            println(if ((n & 1) == 1) -1 else {
                val h = n >>> 1
                val a = Array.fill(26){0}
                for (i <- 0 until h) a(s(i) - 'a') += 1
                for (i <- h until n) a(s(i) - 'a') -= 1
                a.view.map{_.abs}.sum >>> 1
            })
        }
    }
}
