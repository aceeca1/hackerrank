object Solution {
    import io.StdIn._
    import collection.mutable.Buffer

    def main(args:Array[String]) {
        for (i <- 1 to readInt) {
            val s = readLine
            val n = s.size
            var i = 0
            while (i < n && s(i) == s(n - 1 - i)) i += 1
            println(if (i == n) -1 else {
                val s1 = s.substring(i + 1, n - i)
                if (s1 == s1.reverse) i else n - 1 - i
            })
        }
    }
}
