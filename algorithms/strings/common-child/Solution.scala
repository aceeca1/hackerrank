object Solution {
    import io.StdIn._

    def main(args:Array[String]) {
        val s1 = readLine
        val s2 = readLine
        val a = Array.fill(s1.size + 1, s2.size + 1){0}
        for (i <- 1 to s1.size; j <- 1 to s2.size) a(i)(j) = {
            var k = a(i - 1)(j) max a(i)(j - 1)
            if (s1(i - 1) == s2(j - 1)) k max a(i - 1)(j - 1) + 1 else k
        }
        println(a(s1.size)(s2.size))
    }
}
