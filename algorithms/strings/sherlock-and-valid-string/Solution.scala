object Solution {
    import io.StdIn._
    import collection.mutable.Buffer

    def main(args:Array[String]) {
        val a = Array.fill(26){0}
        for (i <- readLine) a(i - 'a') += 1
        def result:String = {
            if (a.forall{i => i == a.head || i == 0}) return "YES"
            val b = a.clone
            b(0.until(b.size).maxBy{b}) -= 1
            if (b.forall{i => i == b.head || i == 0}) return "YES"
            val k = a.indexOf(1)
            if (k == -1) return "NO"
            a(k) -= 1
            if (a.forall{i => i == a.head || i == 0}) "YES" else "NO"
        }
        println(result)
    }
}
