object Solution {
    import io.StdIn._

    def main(args:Array[String]) {
        println(readLine.groupBy{identity}.values.count{
            x => (x.size & 1) > 0
        } match {
            case 0 | 1 => "YES"
            case _ => "NO"
        })
    }
}
