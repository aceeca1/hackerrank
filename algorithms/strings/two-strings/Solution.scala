object Solution {
    import io.StdIn._
    import collection.immutable.BitSet
    
    def main(args:Array[String]) {
        for (i <- 1 to readInt) {
            val b0 = BitSet(readLine.view.map{_.toInt} : _*)
            val b1 = BitSet(readLine.view.map{_.toInt} : _*) 
            println(if ((b0 & b1).nonEmpty) "YES" else "NO")
        }
    }
}
