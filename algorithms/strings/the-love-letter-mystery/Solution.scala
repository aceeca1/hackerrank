object Solution {
    import io.StdIn._

    def main(args:Array[String]) {
        for (i <- 1 to readInt) {
            val s = readLine
            println((s.reverse, s).zipped.map{
                (c1, c2) => math.abs(c1.toInt - c2.toInt)
            }.sum >>> 1)
        }
    }
}
