object Solution {
    import io.StdIn._
    import collection.mutable.BitSet
    import collection.mutable.Buffer
    import collection.mutable.HashMap
    import util.parsing.combinator._

    class Node {
        case class Edge(p:Int, t:Node)
        val e = Buffer[Edge]()
        var num = -1
    };

    case class NFAe(s:Node, t:Node) {
        var a:Buffer[Node] = null
        def number {
            a = Buffer[Node]()
            def visit(n:Node) {
                if (n.num == -1) {
                    n.num = a.size
                    a += n
                    for (i <- n.e) visit(i.t)
                }
            }; visit(s)
        }
        def toDFA:DFA = {
            number
            def eClosure(n:BitSet) = {
                val n0 = BitSet()
                def visit(k:Int) {
                    if (n0.contains(k)) return
                    n0 += k
                    for (i <- a(k).e; if i.p == 0) visit(i.t.num)
                }
                for (i <- n) visit(i); n0
            }
            def go(n:BitSet, d:Int) = {
                val n0 = BitSet()
                for (i <- n; j <- a(i).e; if j.p == d) {
                    n0 += j.t.num
                }; n0
            }
            val nodes = HashMap[BitSet, Int]()
            val adjM = Buffer[Buffer[Int]]()
            var num = 0
            def visit(n:BitSet, p:Int) {
                val n0 = eClosure(n)
                val b = nodes.contains(n0)
                val c = if (b) nodes(n0) else {
                    nodes(n0) = num
                    num += 1
                    for (i <- adjM) i += 0
                    adjM += Buffer.fill(num){0}
                    num - 1
                }
                if (p != -1) adjM(p)(c) += 1
                if (!b) {
                    visit(go(n0, 'a'), c)
                    visit(go(n0, 'b'), c)
                }
            }; visit(BitSet(0), -1)
            val ac = BitSet()
            for ((i, c) <- nodes; if (i.contains(t.num))) ac += c
            DFA(adjM.map{_.toArray}.toArray, ac)
        }
    }

    type Matrix = Array[Array[Int]]

    final def M = 1000000007

    case class DFA(a:Matrix, t:BitSet) {
        def mul(m1:Matrix, m2:Matrix) = {
            val b = Array.fill(a.size, a.size){0}
            for {
                i1 <- 0 until a.size
                i2 <- 0 until a.size
                i3 <- 0 until a.size
            } {
                val m = m1(i1)(i2).toLong * m2(i2)(i3)
                b(i1)(i3) = ((b(i1)(i3) + m) % M).toInt
            }; b
        }
        def pow(n:Int) = {
            var k = n
            var b = Array.fill(a.size, a.size){0}
            for (i <- 0 until a.size) b(i)(i) = 1
            var c = a
            while (k > 0) {
                if ((k & 1) != 0) b = mul(b, c)
                c = mul(c, c)
                k >>>= 1
            };
            t.toIterator.map{i => b(0)(i).toLong}.sum % M
        }
    }

    object ReParsers extends JavaTokenParsers {
        def letter = "[ab]".r ^^ {p => {
            val s = new Node
            val t = new Node
            s.e += s.Edge(p.head, t)
            NFAe(s, t)
        }}
        def reOr = ("(" ~> re <~ "|") ~ (re <~ ")") ^^ {
            case r1 ~ r2 => {
                r1.s.e += r1.s.Edge(0, r2.s)
                r2.t.e += r2.t.Edge(0, r1.t); r1
            }
        }
        def reStar = "(" ~> re <~ "*)" ^^ {
            case r1 => {
                val s = new Node
                val t = new Node
                s.e += s.Edge(0, r1.s)
                r1.s.e += r1.s.Edge(0, r1.t)
                r1.t.e += r1.t.Edge(0, r1.s)
                r1.t.e += r1.t.Edge(0, t)
                NFAe(s, t)
            }
        }
        def reCat = "(" ~> re ~ re <~ ")" ^^ {
            case r1 ~ r2 => {
                r1.t.e += r1.t.Edge(0, r2.s)
                NFAe(r1.s, r2.t)
            }
        }
        def re:Parser[NFAe] = letter | reOr | reStar | reCat
        def run(s:String) = parseAll(re, s).get
    }

    def main(args:Array[String]) {
        for (i <- 1 to readInt) {
            val Array(s, nS) = readLine.split(' ')
            val n = nS.toInt
            println(ReParsers.run(s).toDFA.pow(n))
        }
    }
}
