object Solution {
    import io.StdIn._
    import collection.mutable.Buffer

    def main(args:Array[String]) {
        val g = Buffer.fill(readInt){readLine}.reduce{_ intersect _}
        println(g.distinct.size)
    }
}
