object Solution {
    import io.StdIn._

    def main(args:Array[String]) {
        for (i <- 1 to readInt) {
            println(readLine.foldLeft(('_', 0)){
                case ((x1, s), x2) => (x2, if (x1 == x2) s + 1 else s)
            }._2)
        }
    }
}
