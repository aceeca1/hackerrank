object Solution {
    import io.StdIn._
    import collection.mutable.Buffer

    def main(args:Array[String]) {
        val s = readLine.map{_ - 'a'}
        val a = Array.fill(26){0}
        for (i <- s) a(i) += 1
        for (i <- 0 until 26) a(i) >>>= 1
        val needs = Array.fill(s.size){0}
        val b = Array.fill(26){0}
        for (i <- Range(s.size - 1, -1, -1)) {
            b(s(i)) += 1
            needs(i) = if (b(s(i)) > a(s(i))) b(s(i)) - a(s(i)) else 0
        }
        val next = Array.fill(26){Buffer[Int]()}
        for (i <- 0 until s.size) {
            next(s(i)) += i
        }
        for (i <- 0 until 26) b(i) = 0
        var p = s.size - 1
        var q = s.size - 1
        val r = Array.fill(s.size >>> 1) {
            while (b(s(q)) >= needs(q)) q -= 1
            var k = 0
            while (b(k) >= a(k) || next(k).last < q) k += 1
            val p0 = next(k).last
            while (p >= p0) {
                val nextSP = next(s(p))
                nextSP.remove(nextSP.size - 1)
                p -= 1
            }
            b(k) += 1; k
        }
        println(r.map{i => (i + 'a').toChar}.mkString)
    }
}
