object Solution {
    import io.StdIn._
    import collection.mutable.Buffer
    
    def main(args:Array[String]) {
        for (i <- 1 to readInt) {
            val s = readLine
            val b = (for {
                i <- Buffer.range(0, s.size)
                j <- i + 1 to s.size
            } yield s.substring(i, j).sorted).groupBy{identity}
            println((for ((_, i) <- b) yield {
                val z = i.size
                z * (z - 1) >>> 1
            }).sum)
        }
    }
}
