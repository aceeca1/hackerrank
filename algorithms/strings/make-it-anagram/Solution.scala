object Solution {
    import io.StdIn._

    def main(args:Array[String]) {
        def f(s : String) = {
            val a = Array.fill(26){0}
            for (i <- s) a(i - 'a') = a(i - 'a') + 1
            a
        }
        println((f(readLine), f(readLine)).zipped.map{
            case (x1, x2) if x1 < x2 => x2 - x1
            case (x2, x1) => x2 - x1
        }.sum)
    }
}
