object Solution {
    import io.StdIn._
    import collection.mutable.Buffer
    
    def main(args:Array[String]) {
        for (i <- 1 to readInt) {
            val s = readLine
            val a = Buffer.range(0, s.size - 1).map{i => (s(i) - s(i + 1)).abs}
            println(if (a == a.reverse) "Funny" else "Not Funny")
        }
    }
}
