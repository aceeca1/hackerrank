object Solution {
    import io.StdIn._

    def main(args:Array[String]) {
        for (i <- 1 to readInt) {
            val s1 = readLine
            val s2 = readLine
            val s = s1 + "~" + s2 + "~"
            val sa = new SuffixArray(s.view.map{_.toLong}.toArray)
            var i1 = 0
            var i2 = s1.size + 1
            while (i1 < s1.size && i2 < s.size) {
                if (sa.less(i1, i2)) {
                    print(s(i1))
                    i1 += 1
                } else {
                    print(s(i2))
                    i2 += 1
                }
            }
            while (i1 < s1.size) {print(s1(i1)); i1 += 1}
            while (i2 < s.size - 1) {print(s(i2)); i2 += 1}
            println
        }
    }

    class SuffixArray(a:Array[Long]) {
        val n = a.size
        val m = Math.getExponent(n) + 1
        val b = Array.fill(m, n + 1){0L}
        b(0) = a :+ 0L

        def cityHash(x1:Long, x2:Long) = {
            val kMul = 0x9ddfea08eb382d69L
            var a = x1 * kMul
            a ^= a >>> 47
            var b = (a ^ x2) * kMul
            b ^ (b >>> 47)
        }

        for (i <- 1 until m; j <- 0 until n) b(i)(j) = {
            val j0 = j + (1 << i - 1)
            cityHash(b(i - 1)(j), if (j0 <= n) b(i - 1)(j0) else 0L)
        }

        def lcp(n1:Int, n2:Int) = {
            var k = 0
            for (i <- Range(m - 1, -1, -1)) {
                if (b(i)(n1 + k) == b(i)(n2 + k)) k += 1 << i
            }; k
        }

        def less(n1:Int, n2:Int):Boolean = {
            if (b(0)(n1) < b(0)(n2)) return true
            if (b(0)(n1) > b(0)(n2)) return false
            val k = lcp(n1, n2)
            b(0)(n1 + k) < b(0)(n2 + k)
        }

        lazy val sa = Array.range(0, n + 1).sortWith{less}
    }
}
