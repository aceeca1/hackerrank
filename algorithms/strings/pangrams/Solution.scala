object Solution {
    import io.StdIn._

    def main(args:Array[String]) {
        val n = readLine.filter{_ != ' '}.toUpperCase.groupBy{identity}.size
        println(if (n == 26) "pangram" else "not pangram")
    }
}
