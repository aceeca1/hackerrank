object Solution {
    import io.StdIn._

    def main(args:Array[String]) {
        val n = readInt
        val a = readLine.split(' ').map{_.toInt}.groupBy{_.signum}
        val aD = a.withDefaultValue(Array())
        for (i <- Array(1, -1, 0)) {
            println((aD(i).size.toDouble / n).formatted("%.6f"))
        }
    }
}
