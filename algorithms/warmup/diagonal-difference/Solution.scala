object Solution {
    import io.StdIn._

    def main(args:Array[String]) {
        val n = readInt
        println(Iterator.tabulate(n){i => {
            val a = readLine.split(' ').map{_.toLong}
            a(i) - a(n - 1 - i)
        }}.sum.abs)
    }
}
