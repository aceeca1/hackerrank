object Solution {
    import io.StdIn._

    def main(args:Array[String]) {
        val n = readInt
        for (i <- 1 to n) {
            for (j <- i + 1 to n) print(' ')
            for (j <- 1 to i) print('#')
            println
        }
    }
}
