object Solution {
    import io.StdIn._

    def main(args:Array[String]) {
        val n = readInt
        println(readLine.split(' ').map{_.toLong}.sum)
    }
}
