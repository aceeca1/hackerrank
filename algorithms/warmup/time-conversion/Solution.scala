object Solution {
    import io.StdIn._

    def main(args:Array[String]) {
        val re = "(\\d+)(:\\d+:\\d+)([AP])M".r
        val re(h, ms, t) = readLine
        val hI = h.toInt
        print(t match {
            case "A" if hI == 12 => "00"
            case "A" => h
            case "P" if hI == 12 => "12"
            case "P" => (hI + 12).toString
        })
        println(ms)
    }
}
