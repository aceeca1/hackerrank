object Solution {
    import io.StdIn._

    def main(args:Array[String]) {
        println(1.to(readInt).map{BigInt(_)}.product)
    }
}
