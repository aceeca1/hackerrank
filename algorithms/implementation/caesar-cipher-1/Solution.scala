object Solution {
    import io.StdIn._

    def main(args:Array[String]) {
        val n = readInt
        val s = readLine
        val c = readInt % 26
        for (i <- s) {
            val i0 = (i + c).toChar
            print(if (i.isUpper) {
                if (i0.isUpper) i0 else (i0 - 26).toChar
            } else if (i.isLower) {
                if (i0.isLower) i0 else (i0 - 26).toChar
            } else i)
        }
        println
    }
}
