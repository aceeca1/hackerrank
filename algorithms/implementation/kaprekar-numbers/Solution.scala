object Solution {
    import io.StdIn._

    def kapre(n:Int) = {
        val d = n.toString.size
        val s = (n.toLong * n).toString
        var s0 = s.substring(0, s.size - d)
        if (s0.size == 0) s0 = "0"
        val s1 = s.substring(s.size - d, s.size)
        s0.toInt + s1.toInt == n
    }
    
    def main(args:Array[String]) {
        var isFirst = true
        for (i <- readInt to readInt; if (kapre(i))) {
            if (!isFirst) print(' ')
            isFirst = false
            print(i)
        }
        println(if (isFirst) "INVALID RANGE" else "")
    }
}
