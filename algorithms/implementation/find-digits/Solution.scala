object Solution {
    import io.StdIn._

    def main(args:Array[String]) {
        for (i <- 1 to readInt) {
            val s = readLine
            val n = s.toLong
            println(s.count{c => {
                val x = c.toString.toLong
                x > 0 && n % x == 0
            }})
        }
    }
}
