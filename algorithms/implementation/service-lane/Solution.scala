object Solution {
    import io.StdIn._

    def main(args:Array[String]) {
        val Array(n, t) = readLine.split(' ').map{_.toInt}
        val a = readLine.split(' ').map{_.toInt}
        val m = 1 << (Math.getExponent(n.toFloat) + 1)
        val b = Array.fill(m + m){0}
        System.arraycopy(a, 0, b, m, n)
        for (i <- (m - 1).to(1, -1)) b(i) = b(i + i) min b(i + i + 1)
        for (i <- 1 to t) {
            var Array(p, q) = readLine.split(' ').map{_.toInt}
            var s = Int.MaxValue
            p = p + m - 1
            q = q + m + 1
            while ((p ^ q) != 1) {
                if ((p & 1) == 0) s = s min b(p + 1)
                if ((q & 1) == 1) s = s min b(q - 1)
                p = p >>> 1
                q = q >>> 1
            }
            println(s)
        }
    }
}
