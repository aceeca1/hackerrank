object Solution {
    import io.StdIn._

    val a = Array(
        "zero", "one", "two", "three", "four", "five", "six", "seven", 
        "eight", "nine", "ten", "eleven", "twelve", "thirteen", "fourteen",
        "fifteen", "sixteen", "seventeen", "eighteen", "nineteen", "twenty", 
        "twenty one", "twenty two", "twenty three", "twenty four",
        "twenty five", "twenty six", "twenty seven", "twenty eight",
        "twenty nine")
    
    def main(args:Array[String]) {
        val h = readInt
        println(readInt match {
            case 0 => a(h) + " o' clock"
            case 1 => "one minute past " + a(h)
            case 15 => "quarter past " + a(h)
            case m if m < 30 => a(m) + " minutes past " + a(h)
            case 30 => "half past " + a(h)
            case 45 => "quarter to " + a(h + 1)
            case 59 => "one minute to " + a(h + 1)
            case m => a(60 - m) + " minutes to " + a(h + 1)
        })
    }
}
