object Solution {
    import io.StdIn._
    import Math._

    def main(args:Array[String]) {
        val r = readLine
        val s = sqrt(r.size)
        val b = ceil(s).toInt
        val a = floor(s).toInt
        val p = if (a * b < r.size) a + 1 else a
        val ri = r.toIterator ++ Iterator.continually('\u0000')
        val e = Array.fill(p, b){ri.next}
        println(0.until(b).map{
            i => e.map{_(i)}.filter(_ > 0).mkString("")
        }.mkString(" "))
    }
}
