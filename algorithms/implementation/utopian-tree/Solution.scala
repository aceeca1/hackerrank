object Solution {
    import io.StdIn._

    def main(args:Array[String]) {
        for (i <- 1 to readInt) {
            var odd = false
            println(Iterator.iterate(1){ x => {
                odd = !odd
                if (odd) x + x else x + 1
            }}.drop(readInt).next)
        }
    }
}
