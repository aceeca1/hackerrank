object Solution {
    import io.StdIn._

    def main(args:Array[String]) {
        for (i <- 1 to readInt) {
            val Array(n, c, m) = readLine.split(' ').map{_.toLong}
            println((n / c * m - 1L) / (m - 1L))
        }
    }
}
