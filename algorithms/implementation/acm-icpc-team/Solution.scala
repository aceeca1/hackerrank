object Solution {
    import io.StdIn._
    import collection.immutable.BitSet

    def main(args:Array[String]) {
        val Array(n, m) = readLine.split(' ').map{_.toInt}
        val a = Array.fill(n){
            val s = readLine
            BitSet(0.until(m).filter{s(_) == '1'} : _*)
        }
        val b = for {
            i1 <- 0 until n
            x1 = a(i1)
            i2 <- (i1 + 1) until n
            x2 = a(i2)
        } yield x1.union(x2).size
        val mt = b.max
        val nt = b.count{_ == mt}
        println(mt)
        println(nt)
    }
}
