object Solution {
    import io.StdIn._

    def main(args:Array[String]) {
        for (i <- 1 to readInt) {
            val Array(n, k) = readLine.split(' ').map{_.toInt}
            val a = readLine.split(' ').map{_.toInt}
            println(if (a.count{_ <= 0} < k) "YES" else "NO")
        }
    }
}
