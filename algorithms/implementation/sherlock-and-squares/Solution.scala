object Solution {
    import io.StdIn._
    import math._

    def main(args:Array[String]) {
        for (i <- 1 to readInt) {
            val Array(a, b) = readLine.split(' ').map{_.toDouble}
            println((floor(sqrt(b)) - floor(sqrt(a - 1))).formatted("%.0f"))
        }
    }
}
