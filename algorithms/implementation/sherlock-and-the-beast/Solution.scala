object Solution {
    import io.StdIn._

    def main(args:Array[String]) {
        for (i <- 1 to readInt) {
            val n = readInt
            println(Range(0, 3).indexWhere{x => {
                val no5 = n - 5 * x
                no5 >= 0 && no5 % 3 == 0
            }} match {
                case -1 => "-1"
                case x => {
                    val no3 = 5 * x
                    val no5 = n - no3
                    "5" * no5 + "3" * no3
                }
            })
        }
    }
}
