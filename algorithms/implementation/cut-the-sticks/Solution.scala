object Solution {
    import io.StdIn._

    def main(args:Array[String]) {
        val n = readInt
        val a = readLine.split(' ').map{_.toInt}.sorted
        var x = 0
        for (i <- 0 until n) {
            if (x != a(i)) {
                x = a(i)
                println(n - i)
            }
        }
    }
}
