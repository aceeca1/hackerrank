object Solution {
    import io.StdIn._

    def main(args:Array[String]) {
        val Array(d0, m0, y0) = readLine.split(' ').map{_.toInt}
        val Array(d1, m1, y1) = readLine.split(' ').map{_.toInt}
        println(if (y0 < y1) 0 
        else if (y0 > y1) 10000 
        else if (m0 < m1) 0
        else if (m0 > m1) (m0 - m1) * 500
        else if (d0 <= d1) 0
        else (d0 - d1) * 15)
    }
}
