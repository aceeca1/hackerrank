object Solution {
    import io.StdIn._

    def main(args:Array[String]) {
        val n = readInt
        val a = Array.fill(n){readLine}
        for (i <- 0 until n) {
            for (j <- 0 until n) {
                print(if (
                    (i > 0)     && (a(i)(j) > a(i - 1)(j)) &&
                    (i < n - 1) && (a(i)(j) > a(i + 1)(j)) &&
                    (j > 0)     && (a(i)(j) > a(i)(j - 1)) &&
                    (j < n - 1) && (a(i)(j) > a(i)(j + 1))
                ) 'X' else a(i)(j))
            }
            println
        }
    }
}
