object Solution {
    import io.StdIn._

    def main(args:Array[String]) {
        for (i <- 1 to readInt) {
            val n1 = readInt - 1
            val Array(a, b) = Array(readInt, readInt).sorted
            println((b - a) match {
                case 0 => (n1 * a).toString
                case k => Range.apply(n1 * a, n1 * b + 1, k).mkString(" ")
            })
        }
    }
}
