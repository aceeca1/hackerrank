object Solution {
    import io.StdIn._

    def main(args:Array[String]) {
        for (i <- 1 to readInt) {
            val Array(b, w) = readLine.split(' ').map{_.toLong}
            var Array(x, y, z) = readLine.split(' ').map{_.toLong}
            if (x + z < y) y = x + z
            if (y + z < x) x = y + z
            println(b * x + w * y)
        }
    }
}
