object Solution {
    import io.StdIn._

    def main(args:Array[String]) {
        val s = readInt
        val a = readLine.split(' ').map{_.toInt}
        var p = s - 1
        val x = a(p)
        while (p >= 1 && a(p - 1) >= x) {
            a(p) = a(p - 1)
            println(a.mkString(" "))
            p = p - 1
        }
        a(p) = x
        println(a.mkString(" "))
    }
}
