object Solution {
    import io.StdIn._

    def main(args:Array[String]) {
        val n = readInt
        val a = readLine.split(' ').map{_.toInt}
        val b = Array.fill(100){0}
        for (i <- a) b(i) = b(i) + 1
        println(b.mkString(" "))
    }
}
