object Solution {
    import io.StdIn._

    def main(args:Array[String]) {
        val n = readInt
        val a = readLine.split(' ').map{_.toInt}
        var p = 0
        while (p < n - 1 && a(p) <= a(p + 1)) p = p + 1
        if (p == n - 1) {
            println("yes")
            return
        }
        def aSwap(x1:Int, x2:Int) {
            val t = a(x1)
            a(x1) = a(x2)
            a(x2) = t
        }
        val q = a.indexOf(a.view(p + 1, n).min, p + 1)
        aSwap(p, q)
        var r = (p - 1) max 0
        while (r < n - 1 && a(r) <= a(r + 1)) r = r + 1
        if (r == n - 1) {
            println("yes")
            println("swap " + (p + 1) + " " + (q + 1))
            return
        }
        aSwap(p, q)
        r = p
        while (r < n - 1 && a(r) >= a(r + 1)) r = r + 1
        if (
            (p == 0 || a(p - 1) <= a(r)) &&
            (r == n - 1 || a(p) <= a(r + 1) && {
                var s = r
                while (s < n - 1 && a(s) <= a(s + 1)) s = s + 1
                s == n - 1
            })
        ) {
            println("yes")
            println("reverse " + (p + 1) + " " + (r + 1))
            return
        }
        println("no")
    }
}
