object Solution {
    import io.StdIn._

    def main(args:Array[String]) {
        val n = readInt
        val a = readLine.split(' ').map{_.toInt}.toBuffer
        val x = a.head
        val b = a.filter{_ < x}.+=(x).++=(a.filter{_ > x})
        println(b.mkString(" "))
    }
}
