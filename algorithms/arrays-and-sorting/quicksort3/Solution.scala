object Solution {
    import io.StdIn._
    import collection.mutable.Buffer

    def main(args:Array[String]) {
        val s = readInt
        val b = readLine.split(' ').map{_.toInt}
        def bSwap(p:Int, q:Int) {
            val t = b(p)
            b(p) = b(q)
            b(q) = t
        }
        def f(p:Int, q:Int) {
            val x = b(q)
            var u = p
            for {
                v <- p until q
                if (b(v) < x)
            } {
                bSwap(u, v)
                u = u + 1
            }
            bSwap(u, q)
            println(b.mkString(" "))
            if (p < u - 1) f(p, u - 1)
            if (u + 1 < q) f(u + 1, q)
        }
        f(0, s - 1)
    }
}
