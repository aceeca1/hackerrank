object Solution {
    import io.StdIn._

    def main(args:Array[String]) {
        val v = readInt
        val n = readInt
        val a = readLine.split(' ').map{_.toInt}
        println(java.util.Arrays.binarySearch(a, v))
    }
}
