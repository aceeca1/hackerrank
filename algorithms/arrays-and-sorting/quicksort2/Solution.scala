object Solution {
    import io.StdIn._
    import collection.mutable.Buffer

    def main(args:Array[String]) {
        val n = readInt
        val a = readLine.split(' ').map{_.toInt}.toBuffer
        def f(c:Buffer[Int]) : Iterator[Int] = {
            if (c.size <= 1) return c.toIterator
            val x = c.head
            val b1 = f(c.filter{_ < x})
            val b2 = f(c.filter{_ == x})
            val b3 = f(c.filter{_ > x})
            val b = (b1 ++ b2 ++ b3).toBuffer
            println(b.mkString(" "))
            b.toIterator
        }
        f(a)
    }
}
