object Solution {
    import io.StdIn._

    def main(args:Array[String]) {
        for (i <- 1 to readInt) {
            val n = readInt
            val a = readLine.split(' ').map{_.toInt}
            val u = a.max
            val m = 1 << (Math.getExponent(u.toFloat) + 1)
            val b = Array.fill(m + m){0}
            var s = 0L
            for (i <- a.indices) {
                var p = a(i) + m - 1
                while (p != 1) {
                    if ((p & 1) == 0) s = s + b(p + 1)
                    b(p) = b(p) + 1
                    p = p >>> 1
                }
            }
            println(s)
        }
    }
}
