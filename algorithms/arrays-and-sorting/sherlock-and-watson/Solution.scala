object Solution {
    import io.StdIn._

    def main(args:Array[String]) {
        val Array(n, k, q) = readLine.split(' ').map{_.toInt}
        val a = readLine.split(' ').map{_.toInt}.toBuffer
        val (a1, a2) = a.splitAt(n - k % n)
        val b = a2.++=(a1)
        for (i <- 1 to q) println(b(readInt))
    }
}
