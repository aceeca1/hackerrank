object Solution {
    import io.StdIn._

    def main(args:Array[String]) {
        val n = readInt
        val a = readLine.split(' ').map{_.toInt}.sorted
        val b = Array.tabulate(a.size - 1){i => a(i + 1) - a(i)}
        val m = b.min
        var inTail = false
        for (i <- b.indices) if (b(i) == m) {
            if (inTail) print(' ')
            inTail = true
            print(a(i))
            print(' ')
            print(a(i + 1))
        }
        println
    }
}
