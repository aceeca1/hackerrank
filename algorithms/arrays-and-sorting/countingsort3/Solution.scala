object Solution {
    import io.StdIn._

    def main(args:Array[String]) {
        val n = readInt
        val a = Array.fill(n){readLine.split(' ').head.toInt}
        val b = Array.fill(100){0}
        for (i <- a) b(i) = b(i) + 1
        println(b.scanLeft(0){_ + _}.tail.mkString(" "))
    }
}
