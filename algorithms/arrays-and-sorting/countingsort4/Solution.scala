object Solution {
    import io.StdIn._

    def main(args:Array[String]) {
        val n = readInt
        case class Ele(k:Int, var v:String)
        val a = Array.fill(n){
            val Array(b, c) = readLine.split(' ')
            Ele(b.toInt, c)
        }
        for (i <- 0.until(n / 2)) a(i).v = "-"
        println(a.sortBy(_.k).map{_.v}.mkString(" "))
    }
}
