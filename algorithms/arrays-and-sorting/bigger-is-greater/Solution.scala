object Solution {
    import io.StdIn._
    import collection.mutable.Buffer

    def main(args:Array[String]) {
        for (i <- 1 to readInt) {
            var a = readLine.toBuffer
            var p = a.size - 2
            while (p >= 0 && a(p) >= a(p + 1)) p = p - 1
            println(p match {
                case -1 => "no answer"
                case _ => {
                    val b = a.clone
                    val q = a.lastIndexWhere{_ > a(p)}
                    b(p) = a(q)
                    a(q) = a(p)
                    for (i <- (p + 1) until a.size) {
                        b(a.size + p - i) = a(i)
                    }
                    b.mkString
                }
            })
        }
    }
}
