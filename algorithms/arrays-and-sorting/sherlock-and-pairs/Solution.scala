object Solution {
    import io.StdIn._

    def main(args:Array[String]) {
        for (i <- 1 to readInt) {
            val n = readInt
            val a = readLine.split(' ').map{_.toInt}
            println(a.groupBy{identity}.map{x => {
                val k = x._2.size.toLong
                k * (k - 1)
            }}.sum)
        }
    }
}
