object Solution {
    import io.StdIn._
    import Math._
    import collection.mutable.Buffer

    val n = readInt
    val st = new java.util.StringTokenizer(readLine)
    val a = Array.fill(n){st.nextToken.toInt}
    val ch = Array.fill(n){Buffer[Int]()}
    for (_ <- 1 until n) {
        val Array(u, v) = readLine.split(' ').map{_.toInt}
        ch(u - 1).+=(v - 1)
        ch(v - 1).+=(u - 1)
    }

    def rootify(n:Int, p:Int) {
        val chN = ch(n)
        var i = 0
        while (i < chN.size) {
            if (chN(i) == p) {
                chN(i) = chN(chN.size - 1)
                chN.remove(chN.size - 1)
                i = chN.size
            }
            i = i + 1
        }
        for (i <- chN) rootify(i, n)
    }; rootify(0, -1)

    val s = a.sum
    var m = Int.MaxValue
    def f(i:Int):Int = {
        val r = ch(i).map(f).sum + a(i)
        m = m.min(abs(s - (r << 1))); r
    }; f(0)

    def main(args:Array[String]) {
        println(m)
    }
}
