object Solution {
    import io.StdIn._
    import collection.mutable.Buffer

    val Array(n, k) = readLine.split(' ').map{_.toInt}
    val ch = Array.fill(n){Buffer[Int]()}
    val isRoot = Array.fill(n){true}
    for (_ <- 1 until n) {
        val Array(u, v) = readLine.split(' ').map{_.toInt}
        ch(u - 1).+=(v - 1)
        isRoot(v - 1) = false
    }
    val root = isRoot.indexOf(true)

    var s = 0L
    def tour(p:Int) {
        s = s + (SegTree.rank(p + k + 1) - SegTree.rank(p - k))
        SegTree.add(p, 1)
        for (i <- ch(p)) tour(i)
        SegTree.add(p, -1)
    }; tour(root)

    def main(args:Array[String]) {
        println(s)
    }

    object SegTree {
        val m = 1 << Math.getExponent(n + 1) + 1
        val b = Array.fill(m){0}
        def add(k:Int, d:Int = 1) {
            var p = k + m
            while (p != 1) {
                if ((p & 1) == 0) {
                    p = p >>> 1
                    b(p) = b(p) + d
                } else p = p >>> 1
            }
        }
        def rank(k:Int) = {
            var p = k + m
            if (k > m - 1) p = m + m - 1
            if (k < 0) p = m
            var r = 0
            while (p != 1) {
                if ((p & 1) != 0) {r = r + b(p >>> 1)}
                p = p >>> 1
            }; r
        }
    }
}
