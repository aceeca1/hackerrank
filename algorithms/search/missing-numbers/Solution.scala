object Solution {
    import io.StdIn._
    import collection.mutable.HashSet

    def read = {
        val n = readInt
        val st = new java.util.StringTokenizer(readLine)
        val a = Array.fill(n){st.nextToken.toInt}
        a.groupBy(identity).mapValues{_.size}
    }

    def main(args:Array[String]) {
        val a1 = read.withDefaultValue(0)
        val a2 = read
        val a = for ((k, v) <- a2.toIterator; if (a1(k) != v)) yield k
        val b = a.toBuffer.sorted
        println(b.mkString(" "))
    }
}
