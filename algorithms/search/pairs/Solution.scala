object Solution {
    import io.StdIn._
    import collection.mutable.HashSet

    val Array(n, k) = readLine.split(' ').map{_.toInt}
    val st = new java.util.StringTokenizer(readLine)
    val a = new HashSet[Int]
    for (i <- 1 to n) a.+=(st.nextToken.toInt)

    def main(args:Array[String]) {
        println(a.count{i => a(i - k)})
    }
}
