object Solution {
    import io.StdIn._

    def solve {
        val m = readInt
        val n = readInt
        val st = new java.util.StringTokenizer(readLine)
        val a = Array.fill(n){st.nextToken.toInt}
        val b = a.zipWithIndex.toMap
        for (i <- a.indices) if (b.contains(m - a(i)) && i != b(m - a(i))) {
            print(i + 1); print(' '); println(b(m - a(i)) + 1)
            return
        }
    }

    def main(args:Array[String]) {
        for (_ <- 1 to readInt) solve
    }
}
