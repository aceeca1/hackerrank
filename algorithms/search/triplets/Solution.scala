object Solution {
    import io.StdIn._

    val n = readInt
    val st = new java.util.StringTokenizer(readLine)
    val a = Array.fill(n){st.nextToken.toInt}
    var p = 0; {
        val c = Array.range(0, a.size).sortBy(a)
        var q = a(c.head) + 1
        for (i <- c) {
            if (a(i) != q) {
                q = a(i)
                p = p + 1
            }
            a(i) = p
        }
    }

    object SegTree {
        val m = 1 << Math.getExponent(p + 1) + 1
        val b = Array.fill(m + m){0}
        val c = Array.fill(m + m){0L}
        def visit(k:Int) = {
            var p = k + m
            var sB = 0
            var sC = 0L
            while (p != 1) {
                if ((p & 1) != 0) {
                    sB = sB + b(p ^ 1)
                    sC = sC + c(p ^ 1)
                }
                p = p >>> 1
            }
            p = k + m
            val bU = b(p) == 0
            val cU = sB - c(p)
            while (p != 0) {
                if (bU) b(p) = b(p) + 1
                c(p) = c(p) + cU
                p = p >>> 1
            }; sC
        }
    }

    def main(args:Array[String]) {
        val d = Array.fill(p + 1){0L}
        for (i <- a) d(i) = SegTree.visit(i)
        println(d.sum)
    }
}
