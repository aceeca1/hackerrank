object Solution {
    import io.StdIn._

    def main(args:Array[String]) {
        for (_ <- 1 to readInt) {
            val Array(n, k) = readLine.split(' ').map{_.toInt}
            val st = new java.util.StringTokenizer(readLine)
            val a = Array.fill(n){st.nextToken.toInt}
            var p = -1
            var q = 0
            var s = 0L
            while (q < n) {
                if (a(q) > k) {
                    s = s + (q - p).toLong * (n - q)
                    p = q
                }
                q = q + 1
            }
            println(s)
        }
    }
}
