object Solution {
    import io.StdIn._

    def main(args:Array[String]) {
        for (_ <- 1 to readInt) {
            val Array(n, m) = readLine.split(' ').map{_.toInt}
            val a = Array.fill(n){readLine}
            def aFind(c:Char) = {
                val k = a.map{_.indexOf(c)}
                val kX = k.indexWhere{_ != -1}
                (kX, k(kX))
            }
            val (sX, sY) = aFind('M')
            val (tX, tY) = aFind('*')
            val d = Array((1, 0), (0, 1), (0, -1), (-1, 0))
            def cL(cX:Int, cY:Int, p:Int):Int = {
                if (cX == tX && cY == tY) return 0
                var u = -2
                var w = 0
                for (i <- 0 to 3) if (i != p) {
                    val (nX, nY) = (cX + d(i)._1, cY + d(i)._2)
                    if (
                        0 <= nX && nX < n &&
                        0 <= nY && nY < m &&
                        a(nX)(nY) != 'X'
                    ) {
                        w = w + 1
                        val r = cL(nX, nY, 3 - i)
                        if (r >= 0) u = r
                    }
                }
                if (w >= 2) u + 1 else u
            }
            println(if (cL(sX, sY, -1) == readInt) "Impressed" else "Oops!")
        }
    }
}
