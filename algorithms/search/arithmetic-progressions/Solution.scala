object Solution {
    import io.StdIn._

    final def M = 1000003
    final def M1 = M - 1

    object ModM {
        val facT = Array.fill(M){0}; {
            facT(0) = 1
            var i = 1
            var p = 1L
            while (i < M) {
                p = p * i % M
                facT(i) = p.toInt
                i = i + 1
            }
        }
        def fac(k:Long) = if (k >= M) 0 else facT(k.toInt)
        val log = Array.fill(M){0}
        val twoP = Array.fill(M){0}; {
            var i = 0
            var p = 1
            while (i < M) {
                twoP(i) = p
                log(p) = i
                i = i + 1
                p = p + p
                if (p >= M) p = p - M
            }
        }
    }

    val n = readInt
    object SegTree {
        val m = 1 << Math.getExponent(n + 2) + 1
        val a = Array.fill(m){0L}
        val b = Array.fill(m + m){0L}
        val c = Array.fill(m + m){0L}
        val d = Array.fill(m + m){0L}
        var aL = 0
        var bL = 0
        for (i <- 1 to n) {
            val Array(_, e, p) = readLine.split(' ').map{_.toInt}
            aL = aL + ModM.log(e)
            if (aL >= M1) aL = aL - M1
            a(i) = aL
            b(m + i) = p - bL
            c(m + i) = (i - 1) * b(m + i)
            d(m + i) = (a(i - 1) * b(m + i) % M1 + M1) % M1
            bL = p
        }
        for (i <- Range(m - 1, 0, -1)) {
            b(i) = b(i + i) + b(i + i + 1)
            c(i) = c(i + i) + c(i + i + 1)
            val di = d(i + i) + d(i + i + 1)
            d(i) = if (di >= M1) di - M1 else di
        }
        def query(u:Int) = {
            var p = u + m + 1
            var sB = 0L
            var sC = 0L
            var sD = 0L
            while (p != 1) {
                if ((p & 1) != 0) {
                    sB = sB + b(p ^ 1)
                    sC = sC + c(p ^ 1)
                    sD = sD + d(p ^ 1)
                }
                p = p >>> 1
            }
            val r1 = sB * u - sC
            val r2 = ((sB * a(u) - sD) % M1 + M1) % M1
            (r1, ModM.twoP(r2.toInt))
        }
        def queryI(u:Int, v:Int) {
            val (p1, p2) = query(u - 1)
            val (q1, q2) = query(v)
            val r1 = q1 - p1
            val r2 = q2.toLong * ModM.twoP(M - 1 - ModM.log(p2)) % M
            print(r1)
            print(' ')
            println(r2 * ModM.fac(r1) % M)
        }
        def update(u:Int, bD:Int) {
            val cD = bD * (u - 1)
            val dD = bD * a(u - 1) % M1
            var p = u + m
            while (p != 0) {
                b(p) = b(p) + bD
                c(p) = c(p) + cD
                d(p) = d(p) + dD
                if (d(p) >= M1) d(p) = d(p) - M1
                p = p >>> 1
            }
        }
        def updateI(u:Int, v:Int, bD:Int) {
            update(u, bD)
            update(v + 1, -bD)
        }
    }

    def main(args:Array[String]) {
        SegTree
        for (_ <- 1 to readInt) {
            val r = readLine.split(' ').map{_.toInt}
            r(0) match {
                case 0 => SegTree.queryI(r(1), r(2))
                case 1 => SegTree.updateI(r(1), r(2), r(3))
            }
        }
    }
}
