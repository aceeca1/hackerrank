object Solution {
    import io.StdIn._

    def main(args:Array[String]) {
        val m = readInt
        val n = readInt
        val a = Array.fill(m){readLine.split(' ').map{_.head}}
        def visit(x1:Int, x2:Int):Int = {
            a(x1)(x2) = '0'
            var k = 1
            for {(y1, y2) <- Array(
                (x1 - 1, x2 - 1), (x1 - 1, x2), (x1 - 1, x2 + 1),
                (x1, x2 - 1), (x1, x2), (x1, x2 + 1),
                (x1 + 1, x2 - 1), (x1 + 1, x2), (x1 + 1, x2 + 1))
                if (0 <= y1 && y1 < m && 0 <= y2 && y2 < n)
                if (a(y1)(y2) == '1')
            } k += visit(y1, y2); k
        }
        var maxk = 0
        for (i1 <- 0 until m; i2 <- 0 until n; if (a(i1)(i2) == '1')) {
            val k = visit(i1, i2)
            if (k > maxk) maxk = k
        }
        println(maxk)
    }
}
