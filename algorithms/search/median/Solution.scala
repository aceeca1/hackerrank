object Solution {
    import io.StdIn._

    val n = readInt
    val a = Array.fill(n){0.toChar}
    val b = Array.fill(n){0}
    for (i <- a.indices) {
        val r = readLine.split(' ')
        a(i) = r(0)(0)
        b(i) = r(1).toInt
    }
    val c = Array.range(0, b.size).sortBy(b)
    var p = 0
    var q = b(c.head) + 1
    for (i <- c) {
        if (b(i) != q) {
            q = b(i)
            c(p) = q
            p = p + 1
        }
        b(i) = p
    }

    object SegTree {
        val m = 1 << Math.getExponent(p + 2) + 1
        val b = Array.fill(m){0}
        var a = 0
        def add(k:Int, d:Int = 1):Boolean = {
            if (d < 0 && rank(k) == rank(k + 1)) {
                println("Wrong!")
                return false
            }
            a = a + d
            var p = k + m
            while (p != 1) {
                if ((p & 1) == 0) {
                    p = p >>> 1
                    b(p) = b(p) + d
                } else p = p >>> 1
            }; true
        }
        def rank(k:Int) = {
            var p = k + m
            if (k > m - 1) p = m + m - 1
            if (k < 0) p = m
            var r = 0
            while (p != 1) {
                if ((p & 1) != 0) {r = r + b(p >>> 1)}
                p = p >>> 1
            }; r
        }
        def select(k:Int) = {
            var r = k
            var p = 1
            while (p < m) {
                if (r >= b(p)) {
                    r = r - b(p)
                    p = (p << 1) + 1
                } else {
                    p = p << 1
                }
            }; p - m
        }
        def median = {
            if (a == 0) "Wrong!"
            else if ((a & 1) != 0) c(select(a >>> 1) - 1).toString
            else {
                val b = a >>> 1
                val v1 = c(select(b) - 1)
                val v2 = c(select(b - 1) - 1)
                val v = 0.5 * (v1.toDouble + v2)
                v.formatted(if (((v1 ^ v2) & 1) == 0) "%.0f" else "%.1f")
            }
        }
    }

    def main(args:Array[String]) {
        for (i <- a.indices) {
            if (a(i) match {
                case 'a' => SegTree.add(b(i))
                case 'r' => SegTree.add(b(i), -1)
            }) println(SegTree.median)
        }
    }
}
