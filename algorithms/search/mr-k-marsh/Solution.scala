// Acknowledgement: http://t.cn/RwcbJ53
object Solution {
    import io.StdIn._

    val Array(m, n) = readLine.split(' ').map{_.toInt}
    val r = Array.fill(m){readLine}
    val a = Array.fill(n + 1, m + 1){0}
    for (i <- r.indices; j <- r(0).indices) {
        a(j + 1)(i) = if (r(i)(j) == 'x') a(j)(i) + 1 else a(j)(i)
    }

    var w = 0
    private def f(i:Int, j:Int) {
        val aj = a(j); val aj1 = a(j - 1)
        val ai = a(i); val ai1 = a(i - 1)
        var w0 = -0x1fffffff
        var p = -1
        var k = 0
        while (k < m) {
            if (aj(k) != ai1(k)) {
                if (aj(k) != aj1(k) || ai(k) != ai1(k)) p = -1
            } else if (p >= 0) {
                val z = k - p
                if (z > w0) w0 = z
            } else p = k
            k = k + 1
        }
        w = w.max((w0 + j - i) << 1)
        if (j < n) f(i, j + 1)
        else if (i < n - 1) f(i + 1, i + 2)
    }; f(1, 2)

    def main(args:Array[String]) {
        if (w > 0) println(w) else println("impossible")
    }
}
