object Solution {
    import io.StdIn._

    val n = readInt
    val a = ReadArrayInt(n)
    var d = a.min
    val m = a.max - d
    val b = Array.fill(m + 1){0L}
    var i = a.size - 1
    while (i >= 0) {
        b(a(i) - d) = b(a(i) - d) + 1L
        i = i - 1
    }
    i = 0
    var bL = 0L
    var cL = 0L
    while (i < b.size) {
        bL = bL + b(i)
        cL = cL + bL
        b(i) = cL
        i = i + 1
    }
    val s = b(m) - b(m - 1)
    val d1 = b(m - 1)
    val d0 = m * s - d1

    def main(args:Array[String]) {
        val q = readInt
        val e = ReadArrayInt(q)
        var i = 0
        while (i < q) {
            d = d + e(i)
            println(() match {
                case _ if d >= 0 => d0 + d.toLong * n
                case _ if d <= -m => d1 - (d.toLong + m) * n
                case _ => {
                    d0 + d.toLong * s + (b(-d - 1) << 1)
                }
            })
            i = i + 1
        }
    }

    object ReadArrayInt {
        import io.StdIn._
        def apply(n:Int):Array[Int] = {
            val in = readLine
            var p = 0
            val a = Array.fill(n){0}
            var q = 0
            while (q < n) {
                while (in(p).isWhitespace) p = p + 1
                var x = 0
                var neg = false
                if (in(p) == '-') {
                    neg = true
                    p = p + 1
                }
                while (p < in.size && in(p).isDigit) {
                    x = x * 10 + (in(p) - '0')
                    p = p + 1
                }
                a(q) = if (neg) -x else x
                q = q + 1
            }; a
        }
    }
}
