object Solution {
    import io.StdIn._

    val Array(n, m, k) = readLine.split(' ').map{_.toInt}
    val nm = n * m
    val a = Array.fill(n){readLine}

    def changed(u:Int) = {
        val d = Array.fill(n, m){(0x3fffffff, 0)}
        d(0)(0) = (0, 0)
        val b = new java.util.ArrayDeque[(Int, Int)]
        b.addLast((0, 0))
        while (!b.isEmpty()) {
            val (pX, pY) = b.removeFirst
            for (i <- Array(
                ('U', -1, 0), ('D', 1, 0), ('L', 0, -1), ('R', 0, 1)
            )) {
                val qX = pX + i._2
                val qY = pY + i._3
                if (0 <= qX && qX < n && 0 <= qY && qY < m) {
                    val w = d(pX)(pY)._1 + (if (i._1 == a(pX)(pY)) 1 else u)
                    if (w < d(qX)(qY)._1) {
                        val ch = if (i._1 == a(pX)(pY)) 0 else 1
                        d(qX)(qY) = (w, d(pX)(pY)._2 + ch)
                        val bF = b.peekFirst
                        if (bF == null || w <= d(bF._1)(bF._2)._1) {
                            b.addFirst((qX, qY))
                        } else b.addLast((qX, qY))
                    }
                }
            }
        }
        val c = a.map{_.indexOf('*')}
        val starX = c.indexWhere{_ != -1}
        val starY = c(starX)
        val dist = d(starX)(starY)
        val len = dist._1 - dist._2 * (u - 1)
        if (len > k) -1 else dist._2
    }

    def main(args:Array[String]) {
        println(changed(BinarySearch(2, n + m + 1){changed(_) == -1} - 1))
    }

    object BinarySearch {
        def apply(x1:Int, x2:Int)(f:Int => Boolean):Int = {
            if (x1 == x2) return x1
            val m = (x1 + x2) >>> 1
            if (f(m)) apply(x1, m)(f) else apply(m + 1, x2)(f)
        }
    }
}
