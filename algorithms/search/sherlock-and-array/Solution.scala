object Solution {
    import io.StdIn._

    def main(args:Array[String]) {
        for (i <- 1 to readInt) {
            val n = readInt
            val a = readLine.split(' ').map{_.toInt}
            def f(p:Int, q:Int, x:Int):String = () match {
                case _ if p == q => if (x == 0) "YES" else "NO"
                case _ if x < 0 => f(p + 1, q, x + a(p))
                case _ => f(p, q - 1, x - a(q))
            }
            println(f(0, n - 1, 0))
        }
    }
}
