object Solution {
    import io.StdIn._
    import collection.mutable.Buffer

    def isSquare(n:Int) = {
        val k = Math.sqrt(n).toInt
        k * k == n
    }

    def main(args:Array[String]) {
        for (_ <- 1 to readInt) {
            val Array(r, k) = readLine.split(' ').map{_.toInt}
            var w = if (isSquare(r)) 4 else 0
            if (((r & 1) == 0) && isSquare(r >>> 1)) w = w + 4
            def f(i:Int) {
                val u = r - i * i
                if (u <= i * i) return
                if (isSquare(u)) w = w + 8
                f(i + 1)
            }; f(1)
            if (k >= w) println("possible")
            else println("impossible")
        }
    }
}
