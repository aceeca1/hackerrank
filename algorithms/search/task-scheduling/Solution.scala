object Solution {
    import io.StdIn._

    val n = readInt
    val a = Array.fill(n){readLine.split(' ').map{_.toInt}}
    val aM = a.toIterator.map{_(0)}.max

    object SegTree {
        val m = 1 << Math.getExponent(aM + 1) + 1
        val b = Array.fill(m + m){0}
        def fix(p:Int) {
            val e = b(p).max(b(p ^ 1))
            b(p) = b(p) - e
            b(p ^ 1) = b(p ^ 1) - e
            b(p >>> 1) = b(p >>> 1) + e
        }
        for (i <- 1 until m) b(m + i) = 1 - i
        Range(m + m - 2, 1, -2).foreach(fix)
        def add(k:Int, d:Int) = {
            var p = k + m
            while (p != 1) {
                if ((p & 1) == 0) {
                    b(p ^ 1) = b(p ^ 1) + d
                }
                fix(p)
                p = p >>> 1
            }
            b(1)
        }
    }

    def main(args:Array[String]) {
        for (Array(d, m) <- a) {
            println(SegTree.add(d, m))
        }
    }
}
