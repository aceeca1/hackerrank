object Solution {
    import io.StdIn._

    def main(args:Array[String]) {
        val Array(n, k) = readLine.split(' ').map{_.toInt}
        val a = readLine.split(' ').map{_.toInt}.sorted.toIterator
        println(a.scanLeft(k){(s, x) => s - x}.takeWhile{_ >= 0}.size - 1)
    }
}
