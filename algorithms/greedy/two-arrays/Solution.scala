object Solution {
    import io.StdIn._

    def main(args:Array[String]) {
        for (i <- 1 to readInt) {
            val Array(n, k) = readLine.split(' ').map{_.toInt}
            val a = readLine.split(' ').map{_.toInt}.sorted
            val b = readLine.split(' ').map{_.toInt}.sorted
            println(0.until(n).forall{i => a(i) + b(n - 1 - i) >= k} match {
                case true => "YES"
                case _ => "NO"
            })
        }
    }
}
