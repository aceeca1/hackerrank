object Solution {
    import io.StdIn._
    import math._

    def main(args:Array[String]) {
        val n = readInt
        val a = readLine.split(' ').map{_.toInt}.sorted
        val Array(p, q) = readLine.split(' ').map{_.toInt}
        val b = (for {
            i <- 1.until(n)
            x = (a(i) + a(i - 1)) >>> 1
            if ((p <= x) && (x <= q))
        } yield (a(i - 1) - x, x)).toBuffer
        a.indexWhere{_ >= p} match {
            case -1 => ()
            case 0 => b.+=((p - a(0), p))
            case i => b.+=((-min(a(i) - p, p - a(i - 1)), p))
        }
        a.indexWhere{_ >= q} match {
            case -1 => b.+=((a(n - 1) - q, q))
            case 0 => ()
            case i => b.+=((-min(a(i) - q, q - a(i - 1)), q))
        }
        println(b.min._2)
    }
}
