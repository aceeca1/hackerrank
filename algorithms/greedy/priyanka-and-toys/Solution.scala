object Solution {
    import io.StdIn._

    def main(args:Array[String]) {
        val n = readInt
        val a = readLine.split(' ').map{_.toInt}.sorted
        var i = 0
        var k = 0
        while (i < n) {
            k += 1
            val t = a(i)
            while (i < n && t + 4 >= a(i)) i += 1
        }
        println(k)
    }
}
