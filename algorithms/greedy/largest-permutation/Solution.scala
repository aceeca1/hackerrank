object Solution {
    import io.StdIn._

    def main(args:Array[String]) {
        var Array(n, k) = readLine.split(' ').map{_.toInt}
        val a = readLine.split(' ').map{_.toInt}
        val b = Array.fill(n + 1){0}
        for (i <- a.indices) b(a(i)) = i
        var i = 0
        while (i < n && k > 0) {
            if (a(i) < n - i) {
                a(b(n - i)) = a(i)
                b(a(i)) = b(n - i)
                a(i) = n - i
                b(n - i) = i
                k -= 1
            }
            i += 1
        }
        println(a.mkString(" "))
    }
}
