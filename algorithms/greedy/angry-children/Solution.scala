object Solution {
    import io.StdIn._

    def main(args:Array[String]) {
        val n = readInt
        val u = readInt - 1
        val a = Array.fill(n){readInt}.sorted
        println(0.until(n - u).map{i => a(i + u) - a(i)}.min)
    }
}
