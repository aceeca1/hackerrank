object Solution {
    import io.StdIn._

    def main(args:Array[String]) {
        val Array(n, m) = readLine.split(' ').map{_.toInt}
        val c = Array.fill(n + 2){0L}
        for (i <- 1 to m) {
            val Array(a, b, k) = readLine.split(' ').map{_.toInt}
            c(a) = c(a) + k
            c(b + 1) = c(b + 1) - k
        }
        println(c.scanLeft(0L){_ + _}.max)
    }
}
