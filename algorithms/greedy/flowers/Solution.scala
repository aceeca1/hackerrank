object Solution {
    import io.StdIn._

    def main(args:Array[String]) {
        val Array(n, k) = readLine.split(' ').map{_.toInt}
        val a = readLine.split(' ').map{_.toInt}.sorted
        var p = 1
        var q = 0
        var s = 0
        for (i <- Range(a.size - 1, -1, -1)) {
             s += a(i) * p
             if (q == k - 1) {
                q = 0
                p += 1
             } else q += 1
        }
        println(s)
    }
}
