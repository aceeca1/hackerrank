object Solution {
    import io.StdIn._

    def main(args:Array[String]) {
        val n = readInt
        val a = readLine.split(' ').map{_.toInt}
        var b = 0
        for (i <- Range(n - 1, -1, -1)) {
            b = (b + a(i) + 1) >>> 1
        }
        println(b)
    }
}
