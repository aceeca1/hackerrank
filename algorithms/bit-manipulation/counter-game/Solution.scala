object Solution {
    import io.StdIn._

    def main(args:Array[String]) {
        for (t <- 1 to readInt) {
            val n = BigInt(readLine)
            val a = (n & -n) - 1 + n
            val b = a.bitCount
            println(if ((b & 1) != 0) "Richard" else "Louise")
        }
    }
}
