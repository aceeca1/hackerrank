object Solution {
    import io.StdIn._

    def main(args:Array[String]) {
        readInt; println(readLine.split(' ').map{_.toInt}.reduce{_ ^ _})
    }
}
