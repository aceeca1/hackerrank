object Solution {
    import io.StdIn._

    def main(args:Array[String]) {
        val l = readInt
        val r = readInt
        def f(x:Int, s:Int = 0) : Int = x match {
            case 0 => s
            case _ => f(x >>> 1, s + s + 1)
        }
        println(f(l ^ r))
    }
}
