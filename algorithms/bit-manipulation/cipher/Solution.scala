object Solution {
    import io.StdIn._

    def main(args:Array[String]) {
        val Array(n, k) = readLine.split(' ').map{_.toInt}
        val a = "0" + readLine
        val b = Array.fill(a.size){0}
        for (i <- 1 until b.size) b(i) = a(i) ^ a(i - 1)
        val c = Array.fill(a.size - k + 1){0}
        for (i <- 0 until k) c(i) = b(i)
        for (i <- k until c.size) c(i) = c(i - k) ^ b(i)
        println(c.tail.mkString)
    }
}
