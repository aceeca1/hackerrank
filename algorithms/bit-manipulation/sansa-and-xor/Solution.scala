object Solution {
    import io.StdIn._

    def main(args:Array[String]) {
        for (i <- 1 to readInt) {
            val n = readInt
            val a = readLine.split(' ').map{_.toInt}
            println(if ((n & 1) == 0) "0" else {
                Range(0, n, 2).map(a).reduce(_ ^ _)
            })
        }
    }
}
