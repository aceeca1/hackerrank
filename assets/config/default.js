/* -*- Mode: Javascript; indent-tabs-mode:nil; js-indent-level: 2 -*- */
/* vim: set ts=2 et sw=2 tw=80: */

/*************************************************************
 *
 *  MathJax/config/default.js
 *
 *  This configuration file is loaded when you load MathJax
 *  via <script src="MathJax.js?config=default"></script>
 *
 *  Use it to customize the MathJax settings.  See comments below.
 *
 *  ---------------------------------------------------------------------
 *
 *  Copyright (c) 2009-2015 The MathJax Consortium
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */


/*
 *  This file lists most, but not all, of the options that can be set for
 *  MathJax and its various components.  Some additional options are
 *  available, however, and are listed in the various links at:
 *
 *  http://www.mathjax.org/resources/docs/?configuration.html#configuration-options-by-component
 *
 *  You can add these to the configuration object below if you
 *  want to change them from their default values.
 */

MathJax.Hub.Config({

  tex2jax: {

    //
    //  The delimiters that surround in-line math expressions.  The first in each
    //  pair is the initial delimiter and the second is the terminal delimiter.
    //  Comment out any that you don't want, but be sure there is no extra
    //  comma at the end of the last item in the list -- some browsers won't
    //  be able to handle that.
    //
    inlineMath: [
    ['$','$'],      // uncomment this for standard TeX math delimiters
      ['\\(','\\)']
    ],

    //
    //  The delimiters that surround displayed math expressions.  The first in each
    //  pair is the initial delimiter and the second is the terminal delimiter.
    //  Comment out any that you don't want, but be sure there is no extra
    //  comma at the end of the last item in the list -- some browsers won't
    //  be able to handle that.
    //
    displayMath: [
      ['$$','$$'],
      ['\\[','\\]']
    ],

    //
    //  This value determines whether tex2jax requires braces to be
    //  balanced within math delimiters (which allows for nested dollar
    //  signs).  Set to false to get pre-v2.0 compatibility.  When true,
    //
    //      $y = x^2 \hbox{ when $x > 2$}$.
    //
    //  will be properly handled as a single expression.  When false, it
    //  would be interpreted as two searpate expressions, each with
    //  improperly balanced braces.
    //
    balanceBraces: true,

    //
    //  This array lists the names of the tags whose contents should not be
    //  processed by tex2jax (other than to look for ignore/process classes
    //  as listed below).  You can add to (or remove from) this list to prevent
    //  MathJax from processing mathematics in specific contexts.
    //
    skipTags: ["script","noscript","style","textarea","pre","code","annotation","annotation-xml"],

    //
    //  This is the class name used to mark elements whose contents should
    //  not be processed by tex2jax (other than to look for the
    //  processClass pattern below).  Note that this is a regular
    //  expression, and so you need to be sure to quote any regexp special
    //  characters.  The pattern is automatically preceeded by '(^| )(' and
    //  followed by ')( |$)', so your pattern will have to match full words
    //  in the class name.  Assigning an element this class name will
    //  prevent `tex2jax` from processing its contents.
    //
    ignoreClass: "tex2jax_ignore",

    //
    //  This is the class name used to mark elements whose contents SHOULD
    //  be processed by tex2jax.  This is used to turn on processing within
    //  tags that have been marked as ignored or skipped above.  Note that
    //  this is a regular expression, and so you need to be sure to quote
    //  any regexp special characters.  The pattern is automatically
    //  preceeded by '(^| )(' and followed by ')( |$)', so your pattern
    //  will have to match full words in the class name.  Use this to
    //  restart processing within an element that has been marked as
    //  ignored above.
    //
    processClass: "tex2jax_process",

    //
    //  Set to "true" to allow \$ to produce a dollar without starting in-line
    //  math mode.  If you uncomment the ['$','$'] line above, you should change
    //  this to true so that you can insert plain dollar signs into your documents
    //
    processEscapes: false,

    //
    //  Controls whether tex2jax processes LaTeX environments outside of math
    //  mode.  Set to "false" to prevent processing of environments except within
    //  math mode.
    //
    processEnvironments: true,

    //
    //  Controls whether tex2jax processes \ref{...} commands outside
    //  of math mode.  Set to "false" to prevent processing of \ref
    //  except within math mode.
    //
    processRefs: true,

    //
    //  Controls whether tex2jax inserts MathJax_Preview spans to make a
    //  preview available, and what preview to use, when it locates in-line
    //  and display mathetics on the page.  The default is "TeX", which
    //  means use the TeX code as the preview (until it is processed by
    //  MathJax).  Set to "none" to prevent the previews from being
    //  inserted (the math will simply disappear until it is typeset).  Set
    //  to an array containing the description of an HTML snippet in order
    //  to use the same preview for all equations on the page (e.g., you
    //  could have it say "[math]" or load an image).
    //
    //  E.g.,     preview: ["[math]"],
    //  or        preview: [["img",{src: "http://myserver.com/images/mypic.jpg"}]]
    //
    preview: "none"
  }
});

MathJax.Ajax.loadComplete("[MathJax]/config/default.js");
